/*
 * Copyright © 2012, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 * 
 * The NASA Tensegrity Robotics Toolkit (NTRT) v1 platform is licensed
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
*/

/**
 * @file PrismModel.cpp
 * @brief Contains the definition of the members of the class PrismModel.
 * $Id$
 */

// This module
#include "PrismModel.h"
// This library
#include "core/tgBasicActuator.h"
#include "core/tgRod.h"
#include "tgcreator/tgBuildSpec.h"
#include "tgcreator/tgBasicActuatorInfo.h"
#include "tgcreator/tgRodInfo.h"
#include "tgcreator/tgStructure.h"
#include "tgcreator/tgStructureInfo.h"
// The Bullet Physics library
#include "LinearMath/btVector3.h"
// The C++ Standard Library
#include <stdexcept>

/**
 * Anonomous namespace so we don't have to declare the config in
 * the header.
 */
namespace
{
    /**
     * Configuration parameters so they're easily accessable.
     * All parameters must be positive.
     */
    const struct Config
    {
        double density;
        double radius;
        double stiffness;
        double damping;
        double pretension;
        double triangle_length;
        double triangle_height;
        double prism_height;  
    } c =
   {
       0.16,     // density (mass / length^3)
       0.5,     // radius (length)
       1000.0,   // stiffness (mass / sec^2)
       10.0,     // damping (mass / sec)
       500.0,     // pretension (mass * length / sec^2)
       10.0,     // triangle_length (length)
       10.0,     // triangle_height (length)
       20.0,     // prism_height (length)
  };
} // namespace

PrismModel::PrismModel() :
tgModel() 
{
}

PrismModel::~PrismModel()
{
}

void PrismModel::addNodes(tgStructure& s,
                            double edge,
                            double width,
                            double height)
{
    const double x=16;
    //bottom hexagon starting top left rotating clockwise
    s.addNode(-0.5*x, 0, 0.5*x*sqrt(3)); //0
    s.addNode(0.5*x, 0, 0.5*x*sqrt(3)); //1
    s.addNode(x, 0, 0); //2
    s.addNode(0.5*x, 0, -x*0.5*sqrt(3)); //3
    s.addNode(-0.5*x, 0, -x*0.5*sqrt(3)); //4
    s.addNode(-x, 0, 0); //5
    //middle layer triangle starting at top rotating clockwise
    s.addNode(0, 0.5*sqrt(3)*x, 0.5*x*sqrt(3)+0.5*x/sqrt(3)); //6
    s.addNode(x, 0.5*sqrt(3)*x, -x/sqrt(3)); //7
    s.addNode(-x, 0.5*sqrt(3)*x, -x/sqrt(3)); //8
    // Top layer triangle starting at top rotating clockwise
    s.addNode(0, 1.5*x, x/(sqrt(3))); //9
    s.addNode(0.5*x, 1.5*x, -0.5*x/sqrt(3)); //10
    s.addNode(-0.5*x, 1.5*x, -0.5*x/sqrt(3)); //11
    }


void PrismModel::addRods(tgStructure& s) {
    // base
    s.addPair(0,  1,  "rod");
    s.addPair(1,  2,  "rod");
    s.addPair(2,  3,  "rod");
    s.addPair(3,  4,  "rod");
    s.addPair(4,  5,  "rod");
    s.addPair(5,  0,  "rod");
    s.addPair(0,  6,  "rod");
    s.addPair(1,  6,  "rod");
    s.addPair(2,  7,  "rod");
    s.addPair(3,  7,  "rod");
    s.addPair(4,  8,  "rod");
    s.addPair(5,  8,  "rod");
    s.addPair(6,  9,  "rod");
    s.addPair(7,  10,  "rod");
    s.addPair(8,  11,  "rod");
    s.addPair(9,  10,  "rod");
    s.addPair(10,  11,  "rod");
    s.addPair(11,  9,  "rod");

}

void PrismModel::setup(tgWorld& world)
{
    // Define the configurations of the rods and strings
    // Note that pretension is defined for this string
    const tgRod::Config rodConfig(c.radius, c.density);
   // const tgSpringCableActuator::Config muscleConfig(c.stiffness, c.damping, c.pretension);
    
    // Create a structure that will hold the details of this model
    tgStructure s;
    
    // Add nodes to the structure
    addNodes(s, c.triangle_length, c.triangle_height, c.prism_height);
    
    // Add rods to the structure
    addRods(s);
    
    // Add muscles to the structure
   // addMuscles(s);
    
    // Move the structure so it doesn't start in the ground
    s.move(btVector3(0, 10, 0));
    
    // Create the build spec that uses tags to turn the structure into a real model
    tgBuildSpec spec;
    spec.addBuilder("rod", new tgRodInfo(rodConfig));
    //spec.addBuilder("muscle", new tgBasicActuatorInfo(muscleConfig));
    
    // Create your structureInfo
    tgStructureInfo structureInfo(s, spec);

    // Use the structureInfo to build ourselves
    structureInfo.buildInto(*this, world);

    // We could now use tgCast::filter or similar to pull out the
    // models (e.g. muscles) that we want to control. 
    allActuators = tgCast::filter<tgModel, tgSpringCableActuator> (getDescendants());
    
    // Notify controllers that setup has finished.
    notifySetup();
    
    // Actually setup the children
    tgModel::setup(world);
}

void PrismModel::step(double dt)
{
    // Precondition
    if (dt <= 0.0)
    {
        throw std::invalid_argument("dt is not positive");
    }
    else
    {
        // Notify observers (controllers) of the step so that they can take action
        notifyStep(dt);
        tgModel::step(dt);  // Step any children
    }
}

void PrismModel::onVisit(tgModelVisitor& r)
{
    // Example: m_rod->getRigidBody()->dosomething()...
    tgModel::onVisit(r);
}

const std::vector<tgSpringCableActuator*>& PrismModel::getAllActuators() const
{
    return allActuators;
}
    
void PrismModel::teardown()
{
    notifyTeardown();
    tgModel::teardown();
}
