#ifndef TUCONTROLLER
#define TUCONTROLLER

/**
 * @file tuController.h
 * @brief Contains the definition of the new controller class for TU use
 * @author Guido Tournois
 * @version 1.0.0
 */

#include <vector>
#include <Eigen/Core>
#include "core/tgModel.h"
#include "core/tgSubject.h"

//#include "SDL2/SDL.h"
#include "core/tgObserver.h"

class PrismModel;
class tgSpringCableActuator;

class tuController {
public:
	// Creator
	tuController();

	bool rollFlag = false;

	// destructor
	virtual ~tuController(void){};
//	void onSetup(tgModel& subject);

	void onStep(PrismModel& subject,std::vector<tgSpringCableActuator*> actuators, double dt);

	//	tuController(tgModel& subject,std::vector<tgSpringCableActuator*, std::allocator<tgSpringCableActuator*> > actuators, btDynamicsWorld::STATE state, double dt);

	void roll(PrismModel& subject,std::vector<tgSpringCableActuator*> actuators, double dt);

private:

};

#endif
