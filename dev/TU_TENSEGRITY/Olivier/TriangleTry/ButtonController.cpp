/*
 * Copyright © 2015, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * The NASA Tensegrity Robotics Toolkit (NTRT) v1 platform is licensed
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

/**
 * @file ScarrArmController.cpp
 * @brief Preferred Length Controller for ScarrArmModel
 * @author Steven Lessard
 * @version 1.0.0
 * $Id$
 */

// This module
#include "ButtonController.h"
// This application
#include "TriangleModel.h"
// This library
#include "core/tgBasicActuator.h"
// The C++ Standard Library
#include <cassert>
#include <stdexcept>
#include <vector>
#include <iostream>

# define M_PI 3.14159265358979323846

using namespace std;

//Constructor using the model subject and a single pref length for all muscles.
ButtonController::ButtonController(const double initialLength, double timestep) :
    m_initialLengths(initialLength),
    m_totalTime(0.0),
    dt(timestep) {}

//Fetch all the muscles and set their preferred length
void ButtonController::onSetup(TriangleModel& subject) {
	this->m_totalTime=0.0;
    const double length = .5;
    std::cout << length << std::endl;
	const std::vector<tgBasicActuator*> muscle = subject.getAllMuscles();

    for (size_t i=0; i<muscle.size(); i++) {
		tgBasicActuator * const pMuscle = muscle[i];
		assert(pMuscle != NULL);
		pMuscle->setControlInput(length, dt);
    }
}

// Set target length of each muscle, then move motors accordingly
void ButtonController::onStep(TriangleModel& subject, double dt) {
    // Update controller's internal time
    if (dt <= 0.0) { throw std::invalid_argument("dt is not positive"); }
    m_totalTime+=dt;

    setLength(subject, dt); //pitch
    moveAllMotors(subject, dt);
    //updateActions(dt);
}

void ButtonController::setLength(TriangleModel& subject, double dt) {
    const double mean_length = 4; //TODO: define according to vars
    double newLength = 0;

    const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();

    for (size_t i=0; i<muscles.size(); i++) {
		tgBasicActuator * const pMuscle = muscles[i];
		assert(pMuscle != NULL);
        newLength = 1+sin(m_totalTime);
		pMuscle->setControlInput(newLength, dt);
    }
}


//Move motors for all the muscles
void ButtonController::moveAllMotors(TriangleModel& subject, double dt) {
    const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();
    for (size_t i = 0; i < muscles.size(); ++i) {
		tgBasicActuator * const pMuscle = muscles[i];
		assert(pMuscle != NULL);
		pMuscle->moveMotors(dt);
	}

}


//Scale actions according to Min and Max length of muscles.
vector< vector <double> > ButtonController::transformActions(vector< vector <double> > actions)
{
	double min=6;
	double max=11;
	double range=max-min;
	double scaledAct;
	for(unsigned i=0;i<actions.size();i++) {
		for(unsigned j=0;j<actions[i].size();j++) {
			scaledAct=actions[i][j]*(range)+min;
			actions[i][j]=scaledAct;
		}
	}
	return actions;
}

//Pick particular muscles (according to the structure's state) and apply the given actions one by one
void ButtonController::applyActions(TriangleModel& subject, vector< vector <double> > act)
{
	//Get All the muscles of the subject
	const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();
	//Check if the number of the actions match the number of the muscles
	if(act.size() != muscles.size()) {
		cout<<"Warning: # of muscles: "<< muscles.size() << " != # of actions: "<< act.size()<<endl;
		return;
	}
	//Apply actions (currently in a random order)
	for (size_t i = 0; i < muscles.size(); ++i)	{
		tgBasicActuator * const pMuscle = muscles[i];
		assert(pMuscle != NULL);
		cout<<"i: "<<i<<" length: "<<act[i][0]<<endl;
		pMuscle->setControlInput(act[i][0]);
	}
}
