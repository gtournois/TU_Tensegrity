

// This module
#include "TriangleModel.h"
// This library
#include "core/tgBasicActuator.h"
#include "core/tgRod.h"
#include "tgcreator/tgBuildSpec.h"
#include "tgcreator/tgBasicActuatorInfo.h"
#include "tgcreator/tgRodInfo.h"
#include "tgcreator/tgStructure.h"
#include "tgcreator/tgStructureInfo.h"
// The Bullet Physics library
#include "LinearMath/btVector3.h"
// The C++ Standard Library
#include <stdexcept>

/**
 * Anonomous namespace so we don't have to declare the config in
 * the header.
 */
namespace
{
    /**
     * Configuration parameters so they're easily accessable.
     * All parameters must be positive.
     */
    const struct Config
    {
        double density;
        double radius;
        double stiffness;
        double damping;
        double pretension;
        double triangle_length;
        double triangle_height;
        double prism_height;
    } c =
   {
       0.2,     // density (mass / length^3)
       0.31,     // radius (length)
       1000.0,   // stiffness (mass / sec^2)
       10.0,     // damping (mass / sec)
       500.0,     // pretension (mass * length / sec^2)
       10.0,     // triangle_length (length)
       10.0,     // triangle_height (length)
       20.0,     // prism_height (length)
  };
} // namespace

TriangleModel::TriangleModel() :
tgModel()
{
}

TriangleModel::~TriangleModel()
{
}

void TriangleModel::addNodes(tgStructure& s)
{

    // bottom right
    s.addNode(0, 0, 0); // 1
    // bottom left
    s.addNode( (c.triangle_length/2)*sqrt(3.0), c.triangle_length/2, 0); // 2
    // bottom front
    s.addNode( (c.triangle_length/2)*sqrt(3.0), -c.triangle_length/2, 0); // 3
    // bottom right
    s.addNode(-.5, -.5, -.5); // 1
    // bottom left
    s.addNode( (c.triangle_length/2)*sqrt(3.0)-.5, c.triangle_length/2-.5, -.5); // 2
    // bottom front
    s.addNode( (c.triangle_length/2)*sqrt(3.0)+1, -c.triangle_length/2+1, 1); // 3
}

void TriangleModel::addRods(tgStructure& s)
{
    s.addPair( 0,  1, "rod");
    s.addPair( 1,  2, "rod");
    s.addPair( 2,  0, "rod");
    s.addPair( 3,  4, "massless");
    s.addPair( 4,  5, "massless");
    s.addPair( 5,  3, "massless");
}

void TriangleModel::addMuscles(tgStructure& s)
{
	s.addPair( 1,  5, "muscle");
}

void TriangleModel::setup(tgWorld& world)
{
    // Define the configurations of the rods and strings
    // Note that pretension is defined for this string
    const tgRod::Config rodConfig(c.radius, c.density);
    const tgRod::Config rodConfigMassless(c.radius, 0.00/*c.density*/);
    const tgBasicActuator::Config muscleConfig(c.stiffness, c.damping, c.pretension);

    // Create a structure that will hold the details of this model
    tgStructure s;

    // Add nodes to the structure
    addNodes(s);

    // Add rods to the structure
    addRods(s);

    // Add muscles to the structure
    addMuscles(s);

    // Move the structure so it doesn't start in the ground
    s.move(btVector3(0, 30, 0));

    // Create the build spec that uses tags to turn the structure into a real model
    tgBuildSpec spec;
    spec.addBuilder("rod", new tgRodInfo(rodConfig));
    spec.addBuilder("massless", new tgRodInfo(rodConfigMassless));
    spec.addBuilder("muscle", new tgBasicActuatorInfo(muscleConfig));

    // Create your structureInfo
    tgStructureInfo structureInfo(s, spec);

    // Use the structureInfo to build ourselves
    structureInfo.buildInto(*this, world);

    // We could now use tgCast::filter or similar to pull out the
    // models (e.g. muscles) that we want to control.
    allMuscles = tgCast::filter<tgModel, tgBasicActuator> (getDescendants());

    // Notify controllers that setup has finished.
    notifySetup();
    // Actually setup the children
    tgModel::setup(world);
}

void TriangleModel::step(double dt)
{
    // Precondition
    if (dt <= 0.0)
    {
        throw std::invalid_argument("dt is not positive");
    }
    else
    {
        // Notify observers (controllers) of the step so that they can take action
        notifyStep(dt);
        tgModel::step(dt);  // Step any children
    }
}

void TriangleModel::onVisit(tgModelVisitor& r)
{
    // Example: m_rod->getRigidBody()->dosomething()...
    tgModel::onVisit(r);
}

const std::vector<tgBasicActuator*>& TriangleModel::getAllMuscles() const
{
    return allMuscles;
}

void TriangleModel::teardown()
{
    notifyTeardown();
    tgModel::teardown();
}
