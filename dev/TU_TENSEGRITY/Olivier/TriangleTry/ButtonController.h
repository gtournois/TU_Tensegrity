

#ifndef BUTTON_CONTROLLER_H
#define BUTTON_CONTROLLER_H


// This library
#include "core/tgObserver.h"
#include "learning/Adapters/AnnealAdapter.h"
#include <vector>

// Forward declarations
class TriangleModel;

//namespace std for vectors
using namespace std;

/**
 * Preferred Length Controller for ScarrArmModel. This controllers sets a preferred rest length for the muscles.
 * Constant speed motors are used in muscles to move the rest length to the preffered length over time.
 * The assumption here is that motors are constant speed independent of the tension of the muscles.
 * motorspeed and movemotors are defined at the tgBasicActuator class.
 */
class ButtonController : public tgObserver<TriangleModel>
{
public:

  /**
   * Construct a ScarrArmController with the initial preferred length.
   *
   */

  // Note that currently this is calibrated for decimeters.
	ButtonController(const double prefLength, double timestep);

  /**
   * Nothing to delete, destructor must be virtual
   */
  virtual ~ButtonController() { }

  virtual void onSetup(TriangleModel& subject);

  virtual void onStep(TriangleModel& subject, double dt);

protected:

  virtual vector< vector <double> > transformActions(vector< vector <double> > act);

  virtual void applyActions (TriangleModel& subject, vector< vector <double> > act);

private:
  double m_initialLengths;
  double m_totalTime;
  double dt;
  AnnealAdapter evolutionAdapter;

  void setLength(TriangleModel& subject, double dt);
  void moveAllMotors(TriangleModel& subject, double dt);
  void updateActions(TriangleModel& subject, double dt);
};

#endif // SCARRARM_CONTROLLER_H
