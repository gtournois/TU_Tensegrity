/*
 * Copyright © 2015, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 * 
 * The NASA Tensegrity Robotics Toolkit (NTRT) v1 platform is licensed
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
*/

#ifndef Leg_TEST_CONTROLLER_H
#define Leg_TEST_CONTROLLER_H

/**
 * @file LegReadController.h
 * @brief Contains the definition of class LegReadController
 * @author Olivier Hiemstra
 * @version 1.0.0
 * $Id$
 */

// This library
#include "core/tgObserver.h"
#include "learning/Adapters/AnnealAdapter.h"
#include <vector>

// Forward declarations
class LegModel;

//namespace std for vectors
using namespace std;

/**
 * Preferred Length Controller for LegModel. This controllers sets a preferred rest length for the muscles.
 * Constant speed motors are used in muscles to move the rest length to the preffered length over time.
 * The assumption here is that motors are constant speed independent of the tension of the muscles.
 * motorspeed and movemotors are defined at the tgBasicActuator class.
 */
class LegTestControllerSet : public tgObserver<LegModel>
{
public:
	
  /**
   * Construct a LegController with the initial preferred length.
   *
   */
  
  // Note that currently this is calibrated for decimeters.
	LegTestControllerSet(const double prefLength, double timestep);
    
  /**
   * Nothing to delete, destructor must be virtual
   */
  virtual ~LegTestControllerSet() { }

  virtual void onSetup(LegModel& subject);
    
  virtual void onStep(LegModel& subject, double dt);

protected:

private:
  double m_initialLengths;
  double m_posFreq;
  double m_lengthFreq;
  double m_totalTime;
  int m_stepCounter;
  double dt;
  ofstream myFile;
  std::string filename;
  std::string infilename;
  std::string posfilename;
  bool m_set;
  std::string m_iter;

  void moveAllMotors(LegModel& subject, double dt);
  void readLengths(LegModel& subject, std::string inFileName);
  void readPos(LegModel& subject, std::string posFileName);
  void getLengths(LegModel& subject, double time);
  void reset(LegModel& subject);
};

#endif // Leg_CONTROLLER_H
