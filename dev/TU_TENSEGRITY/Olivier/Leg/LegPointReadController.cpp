/*
 * Copyright © 2015, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 * 
 * The NASA Tensegrity Robotics Toolkit (NTRT) v1 platform is licensed
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

/**
 * @file LegReadControllerSet.cpp
 * @brief Preferred Length Controller for LegModel
 * @author Steven Lessard
 * @version 1.0.0
 * $Id$
 */

// This module
#include "LegPointReadController.h"
// This application
#include "LegModel.h"
// This library
#include "core/tgBasicActuator.h"
// The C++ Standard Library
#include <cassert>
#include <stdexcept>
#include <vector>
#include <stdlib.h>
#include "LinearMath/btVector3.h"
#include <stdio.h>
#include <string.h>


// For user input
#include "BulletDynamics/Dynamics/btDynamicsWorld.h"
#include "tgGlutStuff.cpp"

# define M_PI 3.14159265358979323846 

using namespace std;

//Constructor using the model subject and a single pref length for all muscles.
LegPointReadController::LegPointReadController(const double initialLength,
		double timestep) :
		m_initialLengths(initialLength), m_posFreq(1000.0), m_lengthFreq(3.0), m_totalTime(
				0.0), m_stepCounter(0), dt(timestep), myFile(), filename(
				"/home/tensegribuntu/NTRTsim/src/dev/TU_TENSEGRITY/Olivier/Leg/Lengths/ReadmuscleLengths.txt"), rawfilename(
				"/home/tensegribuntu/NTRTsim/src/dev/TU_TENSEGRITY/Olivier/Leg/Lengths/Database/"), posfilename(
				"/home/tensegribuntu/NTRTsim/src/dev/TU_TENSEGRITY/Olivier/Leg/Lengths/Database/pointList.txt"), checked(false), m_about(
				-4.5), m_range(.5), x(10), y(10), point1(false), point2(false), point3(
				false), point4(false), point5(false), point6(false), next(0), dev(.02){
}

void LegPointReadController::onSetup(LegModel& subject) {
	this->m_totalTime = 0.0;


	myFile.open(filename.c_str());
	if (myFile.is_open()) {
		cout << "View file created \n";
		myFile << "Lengths of Leg Muscles \n";
		myFile.close();
	} else
		cout << "Unable to open file";
	myFile.open(posfilename.c_str());
	if (myFile.is_open()) {
		cout << "View file created \n";
		myFile << "";
		myFile.close();
	} else
		cout << "Unable to open file";
	calcPoints();

	for (int i = 0; i < 10; i++) {
		stringstream ss;
		ss << i;
		string myString = ss.str();
		std::string fullname = rawfilename + myString + ".txt";
		myFile.open(fullname.c_str());
		if (myFile.is_open()) {
			cout << "Raw file created \n";
			myFile << "";
			myFile.close();
		} else
			cout << "Unable to open file";
	}
}

void LegPointReadController::onStep(LegModel& subject, double dt) {
	// Update controller's internal time
	btDynamicsWorld::STATE state =
			gtgDemoApplication->m_dynamicsWorld->getState();
	if (dt <= 0.0) {
		throw std::invalid_argument("dt is not positive");
	}
	m_totalTime += dt;
	const std::vector<tgRod*> rods1 = subject.getPointer();
	tgRod* const pointer1 = rods1[rods1.size() - 1];

	if (m_stepCounter == m_posFreq) {
		cout << pointer1->centerOfMass().getY() << ", "
				<< pointer1->centerOfMass().getZ() << "\n";
		cout << y[next] << ", " << x[next] <<"  "<< next <<"\n";
		m_stepCounter = -1;
	}
	m_stepCounter++;

	if (pointer1->centerOfMass().getY() > y[0] - dev
			&& pointer1->centerOfMass().getY() < y[0] + dev
			&& pointer1->centerOfMass().getZ() > x[0] - dev
			&& pointer1->centerOfMass().getZ() < x[0] + dev
			&& next==0) {
		getLengths(subject, m_totalTime, "1");
		//readPos(subject, posfilename);
		point1 = true;
		next++;
	}
	if (pointer1->centerOfMass().getY() > y[1] - dev
				&& pointer1->centerOfMass().getY() < y[1] + dev
				&& pointer1->centerOfMass().getZ() > x[1] - dev
				&& pointer1->centerOfMass().getZ() < x[1] + dev
				&& next == 1) {
			getLengths(subject, m_totalTime, "2");
			//readPos(subject, posfilename);
			point2 = true;
			next++;
		}

	if (pointer1->centerOfMass().getY() > y[2] - dev
			&& pointer1->centerOfMass().getY() < y[2] + dev
			&& pointer1->centerOfMass().getZ() > x[2] - dev
			&& pointer1->centerOfMass().getZ() < x[2] + dev
			&& next == 2) {
		getLengths(subject, m_totalTime, "3");
		//readPos(subject, posfilename);
		point3 = true;
		next++;
	}

	if (pointer1->centerOfMass().getY() > y[3] - dev
			&& pointer1->centerOfMass().getY() < y[3] + dev
			&& pointer1->centerOfMass().getZ() > x[3] - dev
			&& pointer1->centerOfMass().getZ() < x[3] + dev
			&& next == 3) {
		getLengths(subject, m_totalTime, "4");
		//readPos(subject, posfilename);
		point4 = true;
		next++;
	}
	if (pointer1->centerOfMass().getY() > y[4] - dev
			&& pointer1->centerOfMass().getY() < y[4] + dev
			&& pointer1->centerOfMass().getZ() > x[4] - dev
			&& pointer1->centerOfMass().getZ() < x[4] + dev
			&& next == 4) {
		getLengths(subject, m_totalTime, "5");
		//readPos(subject, posfilename);
		point5 = true;
		next++;
	}
	if (pointer1->centerOfMass().getY() > y[5] - dev
			&& pointer1->centerOfMass().getY() < y[5] + dev
			&& pointer1->centerOfMass().getZ() > x[5] - dev
			&& pointer1->centerOfMass().getZ() < x[5] + dev
			&& next == 5) {
		getLengths(subject, m_totalTime, "6");
		//readPos(subject, posfilename);
		point6 = true;
		next++;
	}
	if (pointer1->centerOfMass().getY() > y[6] - dev
				&& pointer1->centerOfMass().getY() < y[6] + dev
				&& pointer1->centerOfMass().getZ() > x[6] - dev
				&& pointer1->centerOfMass().getZ() < x[6] + dev
				&& next == 6) {
			getLengths(subject, m_totalTime, "7");
			//readPos(subject, posfilename);
			point6 = true;
			next++;
		}
	if (pointer1->centerOfMass().getY() > y[7] - dev
				&& pointer1->centerOfMass().getY() < y[7] + dev
				&& pointer1->centerOfMass().getZ() > x[7] - dev
				&& pointer1->centerOfMass().getZ() < x[7] + dev
				&& next == 7) {
			getLengths(subject, m_totalTime, "8");
			//readPos(subject, posfilename);
			point6 = true;
			next++;
		}
	if (pointer1->centerOfMass().getY() > y[8] - dev
				&& pointer1->centerOfMass().getY() < y[8] + dev
				&& pointer1->centerOfMass().getZ() > x[8] - dev
				&& pointer1->centerOfMass().getZ() < x[8] + dev
				&& next == 8) {
			getLengths(subject, m_totalTime, "9");
			//readPos(subject, posfilename);
			point6 = true;
			next++;
		}
	if (pointer1->centerOfMass().getY() > y[9] - dev
				&& pointer1->centerOfMass().getY() < y[9] + dev
				&& pointer1->centerOfMass().getZ() > x[9] - dev
				&& pointer1->centerOfMass().getZ() < x[9] + dev
				&& next == 9) {
			getLengths(subject, m_totalTime, "10");
			//readPos(subject, posfilename);
			point6 = true;
			next++;
		}

	if (next==10) {
		cout << "All points done! \n";
	}
}

void LegPointReadController::getLengths(LegModel& subject, double time,
		std::string iter) {

	myFile.open(filename.c_str(), ios::app);
	myFile << "time:  " << time << "\n";
	myFile << "  Length          Tension          RestLength \n";

	const std::vector<tgBasicActuator*> muscles = subject.find<tgBasicActuator>(
			"active");
	for (size_t i = 0; i < muscles.size(); ++i) {
		tgBasicActuator * const pMuscle = muscles[i];
		assert(pMuscle != NULL);
		myFile << i << ". " << pMuscle->getCurrentLength() << "          "
				<< pMuscle->getTension() << "          "
				<< pMuscle->getRestLength() << "\n";
	}
	myFile << "\n";
	myFile.close();
	std::string fullname = rawfilename + iter + ".txt";
	myFile.open(fullname.c_str(), ios::app);
	for (size_t i = 0; i < muscles.size(); ++i) {
		tgBasicActuator * const pMuscle = muscles[i];
		assert(pMuscle != NULL);
		myFile << pMuscle->getCurrentLength() << "\n";
	}
	myFile.close();
	cout << "Got lengths for point " << iter << "\n";
}

void LegPointReadController::readPos(LegModel& subject, std::string posFileName) {
	const std::vector<tgRod*> rods = subject.getPointer();
	tgRod* const pointer = rods[rods.size() - 1];

	myFile.open(posfilename.c_str(), ios::app);
	myFile << pointer->centerOfMass().getZ() << "\n";

	myFile.close();
	cout << "pos read \n";
}

void LegPointReadController::calcPoints() {
	myFile.open(posfilename.c_str(), ios::app);
	for (int i = 0; i < 10; i++) {
		double factor=1/.95;
		x[i]=-3.8*factor+((3.7*factor+3.8*factor)/9)*i;
		myFile<<x[i]<<"\n";
	}


	for (int i = 0; i < 10; i++) {
//		y[i] = 9.02
//				- 9.11 * pow(10, -9)
//						* sqrt(
//								-12046830000000000 * pow(x[i], 2)
//										- 2318226912318000 * x[i]
//										+ 527226409652854592);
		if(x[i]<0){
		y[i]=1.55*0.1124*pow(x[i],2)+0.1725*x[i]+2.345;
		}
		if(x[i]>=0){
		y[i]=1.25*0.1124*pow(x[i],2)+0.1725*x[i]+2.345;
		}
	}

		myFile.close();

//	for(int i=2;i<4;i++){
//		y[i]=1.35*0.1124*pow(x[i],2)+0.1725*x[i]+2.345;
//	}
//	for(int i=4;i<6;i++){
//			y[i]=1*0.1124*pow(x[i],2)+0.1725*x[i]+2.345;
//		}
}
