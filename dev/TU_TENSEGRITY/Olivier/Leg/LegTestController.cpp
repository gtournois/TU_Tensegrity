/*
 * Copyright © 2015, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 * 
 * The NASA Tensegrity Robotics Toolkit (NTRT) v1 platform is licensed
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

/**
 * @file LegTestController.cpp
 * @brief Preferred Length Controller for LegModel
 * @author Steven Lessard
 * @version 1.0.0
 * $Id$
 */

// This module
#include "LegTestController.h"
// This application
#include "LegModel.h"
// This library
#include "core/tgBasicActuator.h"
// The C++ Standard Library
#include <cassert>
#include <stdexcept>
#include <vector>
#include <stdlib.h>
#include "LinearMath/btVector3.h"


# define M_PI 3.14159265358979323846 

using namespace std;

//Constructor using the model subject and a single pref length for all muscles.
LegTestController::LegTestController(const double initialLength, double timestep) :
    m_initialLengths(initialLength),
	m_posFreq(5.0),
	m_lengthFreq(3.0),
    m_totalTime(0.0),
	m_stepCounter(0),
    dt(timestep),
	myFile (),
	infilename("/home/tensegribuntu/NTRTsim/src/dev/TU_TENSEGRITY/Olivier/Leg/RawmuscleLengths.txt"),
	posfilename("/home/tensegribuntu/Desktop/positionOut.txt"),
	m_set(false){}

void LegTestController::onSetup(LegModel& subject) {
	this->m_totalTime=0.0;

	myFile.open(posfilename);
	if (myFile.is_open())
			  {
				cout << "Position file created \n";
			    myFile << "";
			    myFile.close();
			  }
	else cout << "Unable to open file";

}


void LegTestController::onStep(LegModel& subject, double dt) {
    // Update controller's internal time
    if (dt <= 0.0) { throw std::invalid_argument("dt is not positive"); }
    m_totalTime+=dt;


    if (m_stepCounter==m_posFreq){
    	//readPos(subject, posfilename);
    	m_stepCounter=-1;
    }
    m_stepCounter++;

    if(m_totalTime>3){
    	readLengths(subject, infilename);
    	moveAllMotors(subject, dt);
    }
}

void LegTestController::moveAllMotors(LegModel& subject, double dt) {
    const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();
    for (size_t i = 0; i < muscles.size(); ++i) {
		tgBasicActuator * const pMuscle = muscles[i];
		assert(pMuscle != NULL);
		pMuscle->moveMotors(dt);
	}
}

void LegTestController::readLengths(LegModel& subject, char* inFileName){
	ifstream in;
	string temp;
	in.open(inFileName);
	std::vector<double> lengths;

	while ( in.good() ){
		getline (in,temp);
		lengths.push_back(atof(temp.c_str()));
	}
	in.close();

	const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();
	for (size_t i=0; i<muscles.size(); i++) {
		 tgBasicActuator * const pMuscle = muscles[i];
		 assert(pMuscle != NULL);
		 pMuscle->setControlInput(lengths[i], dt);
	}
}

//void LegTestController::readPos(LegModel& subject, char* posFileName){
//	const std::vector<tgRod*> rods = subject.getPointer();
//	tgRod* const pointer=rods[rods.size()-1];
//
//	myFile.open(posfilename, ios::app);
//	myFile <<pointer->centerOfMass().getX()<<", "<< pointer->centerOfMass().getY()<<", "<< pointer->centerOfMass().getZ() << "\n";
//	myFile.close();
//}
