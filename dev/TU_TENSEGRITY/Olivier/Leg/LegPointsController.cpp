/*
 * Copyright © 2015, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 * 
 * The NASA Tensegrity Robotics Toolkit (NTRT) v1 platform is licensed
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

/**
 * @file LegTestController.cpp
 * @brief Preferred Length Controller for LegModel
 * @author Steven Lessard
 * @version 1.0.0
 * $Id$
 */

// This module
#include "LegPointsController.h"

#include "LegModel.h"
// This library
#include "core/tgBasicActuator.h"
// The C++ Standard Library
#include <cassert>
#include <stdexcept>
#include <vector>
#include <stdlib.h>
#include "LinearMath/btVector3.h"
#include <iostream>

# define M_PI 3.14159265358979323846 

// For user input
#include "BulletDynamics/Dynamics/btDynamicsWorld.h"
#include "tgGlutStuff.cpp"

using namespace std;

//Constructor using the model subject and a single pref length for all muscles.
LegPointsController::LegPointsController(const double initialLength,
		double timestep) :
		m_posFreq(500.0), m_totalTime(0.0), m_stepCounter(0), dt(timestep), myFile(), databaseDir(
				"/home/tensegribuntu/NTRTsim/src/dev/TU_TENSEGRITY/Olivier/Leg/Lengths/Database/"), target(), target_x(), upper_x(), lower_x(), noInter(), m_muscleLengths(), upperNum(), lowerNum() {
}

void LegPointsController::onSetup(LegModel& subject) {
	this->m_totalTime = 0.0;
	target = false;
	noInter = false;
	setTarget();
}

void LegPointsController::onStep(LegModel& subject, double dt) {
	// Update controller's internal time
	if (dt <= 0.0) {
		throw std::invalid_argument("dt is not positive");
	}
	m_totalTime += dt;
	const std::vector<tgRod*> rods1 = subject.getPointer();
		tgRod* const pointer1 = rods1[rods1.size() - 1];
	if (m_stepCounter == m_posFreq) {
		cout << pointer1->centerOfMass().getY() << ", "
						<< pointer1->centerOfMass().getZ() << "\n";
		m_stepCounter = -1;
	}
	m_stepCounter++;

	btDynamicsWorld::STATE state =
			gtgDemoApplication->m_dynamicsWorld->getState();

//	if (!target) {
//		setTarget();
//	}
	if (target) {
		choosePoints();
		interpolateLengths();
		setLengths(subject);
		target=false;
	}
	moveAllMotors(subject,dt);
}

void LegPointsController::moveAllMotors(LegModel& subject, double dt) {
	const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();
	for (size_t i = 0; i < muscles.size(); ++i) {
		tgBasicActuator * const pMuscle = muscles[i];
		assert(pMuscle != NULL);
		pMuscle->moveMotors(dt);
	}
}

void LegPointsController::setTarget() {
	cout << "which x would you like to move to? \n";
	cout << "x: \n";
	double in_x;
	cin >> in_x;
	target_x=(1/(0.95))*in_x;
	target=true;
}

void LegPointsController::choosePoints() {
	std::vector<double> pointOpts;
	ifstream in;
	string temp;
	std::string fullname = databaseDir + "pointList.txt";
	in.open(fullname.c_str());
	while (in.good()) {
		getline(in, temp);
		pointOpts.push_back(atof(temp.c_str()));
	}
	in.close();
	double difUp = 10;
	double difDown = 10;
	for (int i = 0; i < pointOpts.size(); i++) {
		if (pointOpts[i] > target_x) {
			if (abs(pointOpts[i] - target_x) < difUp) {
				difUp = pointOpts[i] - target_x;
				upper_x = pointOpts[i];
				upperNum=i;
			}
		}
		if (pointOpts[i] < target_x) {
			if (abs(pointOpts[i] - target_x) < difDown) {
				difDown = pointOpts[i] - target_x;
				lower_x = pointOpts[i];
				lowerNum=i;
			}
		}
		if (pointOpts[i] == target_x) {
			upperNum=i;
			noInter = true;
		}
	}
}

void LegPointsController::interpolateLengths() {
	if(!noInter){
	std::vector<double> upperMuscles;
	std::vector<double> lowerMuscles;

	upperMuscles = readMuscles(upperNum+1);
	lowerMuscles = readMuscles(lowerNum+1);
	std::vector<double> muscleLengths(upperMuscles.size());

	double ratio = (target_x - lower_x) / (upper_x - lower_x);

	for (int i = 0; i < upperMuscles.size(); i++) {
		muscleLengths[i] = lowerMuscles[i]
				+ (upperMuscles[i] - lowerMuscles[i]) * ratio;
	}
	m_muscleLengths = muscleLengths;
	}
	if(noInter){
			m_muscleLengths = readMuscles(upperNum+1);
	}
}

std::vector<double> LegPointsController::readMuscles(int fileNum) {
	ifstream in;
	string temp;
	std::string fullname = databaseDir + static_cast<ostringstream*>( &(ostringstream() << fileNum) )->str() + ".txt";
	in.open(fullname.c_str());
	std::vector<double> lengths;

	while (in.good()) {
		getline(in, temp);
		lengths.push_back(atof(temp.c_str()));
	}
	in.close();

	return lengths;
}

void LegPointsController::setLengths(LegModel& subject) {
	const std::vector<tgBasicActuator*> muscles = subject.find<tgBasicActuator>(
			"active");
	for (size_t i = 0; i < muscles.size(); i++) {
		tgBasicActuator * const pMuscle = muscles[i];
		assert(pMuscle != NULL);
		pMuscle->setControlInput(m_muscleLengths[i], dt);
	}
}
