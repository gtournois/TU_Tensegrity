/*
 * Copyright © 2015, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 * 
 * The NASA Tensegrity Robotics Toolkit (NTRT) v1 platform is licensed
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

/**
 * @file LegTestController.cpp
 * @brief Preferred Length Controller for LegModel
 * @author Steven Lessard
 * @version 1.0.0
 * $Id$
 */

// This module
#include "LegTestControllerSet.h"

#include "LegModel.h"
// This library
#include "core/tgBasicActuator.h"
// The C++ Standard Library
#include <cassert>
#include <stdexcept>
#include <vector>
#include <stdlib.h>
#include "LinearMath/btVector3.h"
#include <iostream>

# define M_PI 3.14159265358979323846 

// For user input
#include "BulletDynamics/Dynamics/btDynamicsWorld.h"
#include "tgGlutStuff.cpp"

using namespace std;

//Constructor using the model subject and a single pref length for all muscles.
LegTestControllerSet::LegTestControllerSet(const double initialLength, double timestep) :
    m_initialLengths(initialLength),
	m_posFreq(500.0),
	m_lengthFreq(3.0),
    m_totalTime(0.0),
	m_stepCounter(0),
    dt(timestep),
	myFile (),
	filename("/home/tensegribuntu/NTRTsim/src/dev/TU_TENSEGRITY/Olivier/Leg/Lengths/TestMuscleLengths.txt"),
	infilename("/home/tensegribuntu/NTRTsim/src/dev/TU_TENSEGRITY/Olivier/Leg/Lengths/RawmuscleLengths"),
	posfilename("/home/tensegribuntu/Desktop/OutputX2.txt"),
	m_set(false),
	m_iter(){}

void LegTestControllerSet::onSetup(LegModel& subject) {
	this->m_totalTime=0.0;
	cin>>m_iter;
	m_set=false;
}


void LegTestControllerSet::onStep(LegModel& subject, double dt) {
    // Update controller's internal time
    if (dt <= 0.0) { throw std::invalid_argument("dt is not positive"); }
    m_totalTime+=dt;


    if (m_stepCounter==m_posFreq){

    	m_stepCounter=-1;
    }
    m_stepCounter++;

    btDynamicsWorld::STATE state =
    			gtgDemoApplication->m_dynamicsWorld->getState();

    if(m_totalTime>.5){
    	readLengths(subject, infilename);
    	moveAllMotors(subject, dt);
    }

    if (state==7 && !m_set){
           	getLengths(subject, m_totalTime);
           	readPos(subject, posfilename);
           	cout<<"get lengths \n";
           	moveAllMotors(subject, dt);
           	m_set=true;
         }
}

void LegTestControllerSet::moveAllMotors(LegModel& subject, double dt) {
    const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();
    for (size_t i = 0; i < muscles.size(); ++i) {
		tgBasicActuator * const pMuscle = muscles[i];
		assert(pMuscle != NULL);
		pMuscle->moveMotors(dt);
	}
}

void LegTestControllerSet::readLengths(LegModel& subject, std::string inFileName){
	ifstream in;
	string temp;
	std::string fullname=inFileName+m_iter+".txt";
	in.open(fullname.c_str());
	std::vector<double> lengths;

	while ( in.good() ){
		getline (in,temp);
		lengths.push_back(atof(temp.c_str()));
	}
	in.close();

	const std::vector<tgBasicActuator*> muscles = subject.find<tgBasicActuator>("active");
	for (size_t i=0; i<muscles.size(); i++) {
		 tgBasicActuator * const pMuscle = muscles[i];
		 assert(pMuscle != NULL);
		 pMuscle->setControlInput(lengths[i], dt);
	}
}

void LegTestControllerSet::getLengths(LegModel& subject, double time){

		myFile.open(filename.c_str(), ios::app);
		myFile << "time:  " << time<< "\n";
		myFile << "  Length          Tension          RestLength \n";

		const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();
		for (size_t i = 0; i < muscles.size(); ++i) {
				tgBasicActuator * const pMuscle = muscles[i];
				assert(pMuscle != NULL);
				myFile << i << ". "<< pMuscle->getCurrentLength()<< "          "<< pMuscle->getTension()<< "          "<< pMuscle->getRestLength()<< "\n" ;
		}
		myFile<< "\n";
		myFile.close();

}

void LegTestControllerSet::readPos(LegModel& subject, std::string posFileName){
	const std::vector<tgRod*> rods = subject.getPointer();
	tgRod* const pointer=rods[rods.size()-1];

	myFile.open(posfilename.c_str(), ios::app);
	myFile <<pointer->centerOfMass().getZ()<<", "<< pointer->centerOfMass().getY()<<", "<< pointer->centerOfMass().getX() << "\n";
	cout<<pointer->centerOfMass().getY()<<", "<< pointer->centerOfMass().getZ() << "\n";
	myFile.close();
}

void LegTestControllerSet::reset(LegModel& subject){
	const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();
	for (size_t i=0; i<muscles.size(); i++) {
			 tgBasicActuator * const pMuscle = muscles[i];
			 assert(pMuscle != NULL);
			 pMuscle->setControlInput(pMuscle->getStartLength(), dt);
		}
}
