/*
 * Copyright © 2015, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 * 
 * The NASA Tensegrity Robotics Toolkit (NTRT) v1 platform is licensed
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

/**
 * @file LegReadController.cpp
 * @brief Preferred Length Controller for LegModel
 * @author Steven Lessard
 * @version 1.0.0
 * $Id$
 */

// This module
#include "LegReadController.h"
// This application
#include "LegModel.h"
// This library
#include "core/tgBasicActuator.h"
// The C++ Standard Library
#include <cassert>
#include <stdexcept>
#include <vector>
#include <stdlib.h>
#include "LinearMath/btVector3.h"

// For user input
#include "BulletDynamics/Dynamics/btDynamicsWorld.h"
#include "tgGlutStuff.cpp"

# define M_PI 3.14159265358979323846 

using namespace std;

//Constructor using the model subject and a single pref length for all muscles.
LegReadController::LegReadController(const double initialLength,
		double timestep) :
		m_initialLengths(initialLength), m_posFreq(500.0), m_lengthFreq(3.0), m_totalTime(
				0.0), m_stepCounter(0), dt(timestep), myFile(), filename(
				"/home/tensegribuntu/NTRTsim/src/dev/TU_TENSEGRITY/Olivier/Leg/ReadmuscleLengths.txt"), rawfilename(
				"/home/tensegribuntu/NTRTsim/src/dev/TU_TENSEGRITY/Olivier/Leg/RawmuscleLengths.txt"), posfilename(
				"/home/tensegribuntu/Desktop/InputRun3.txt"), checked(false) {
}

void LegReadController::onSetup(LegModel& subject) {
	this->m_totalTime = 0.0;
	myFile.open(filename);
	if (myFile.is_open()) {
		cout << "View file created \n";
		myFile << "Lengths of Leg Muscles \n";
		myFile.close();
	} else
		cout << "Unable to open file";

	myFile.open(rawfilename);
	if (myFile.is_open()) {
		cout << "Raw file created \n";
		myFile << "";
		myFile.close();
	} else
		cout << "Unable to open file";

	// m_state = gtgDemoApplication->m_dynamicsWorld->getState();
}

void LegReadController::onStep(LegModel& subject, double dt) {
	// Update controller's internal time
	btDynamicsWorld::STATE state =
					gtgDemoApplication->m_dynamicsWorld->getState();
	if (dt <= 0.0) {
		throw std::invalid_argument("dt is not positive");
	}
	m_totalTime += dt;

	if (m_stepCounter == m_posFreq) {
		const std::vector<tgRod*> rods1 = subject.getPointer();
		tgRod* const pointer1 = rods1[rods1.size() - 1];
		cout << pointer1->centerOfMass().getY() << ", "
				<< pointer1->centerOfMass().getZ() << "\n";
		m_stepCounter = -1;
	}

	if (!checked) {

		if (state == 7) {
			getLengths(subject, m_totalTime);
			readPos(subject, posfilename);
			cout << "get lengths \n";
			checked=true;
		}
	}
	if(state==8){
		checked=false;
	}

	m_stepCounter++;
}

void LegReadController::getLengths(LegModel& subject, double time) {

	myFile.open(filename, ios::app);
	myFile << "time:  " << time << "\n";
	myFile << "  Length          Tension          RestLength \n";

	const std::vector<tgBasicActuator*> muscles = subject.find<tgBasicActuator>(
			"active");
	for (size_t i = 0; i < muscles.size(); ++i) {
		tgBasicActuator * const pMuscle = muscles[i];
		assert(pMuscle != NULL);
		myFile << i << ". " << pMuscle->getCurrentLength() << "          "
				<< pMuscle->getTension() << "          "
				<< pMuscle->getRestLength() << "\n";
	}
	myFile << "\n";
	myFile.close();

	myFile.open(rawfilename, ios::app);
	for (size_t i = 0; i < muscles.size(); ++i) {
		tgBasicActuator * const pMuscle = muscles[i];
		assert(pMuscle != NULL);
		myFile << pMuscle->getCurrentLength() << "\n";
	}
	myFile.close();
}

void LegReadController::readPos(LegModel& subject, char* posFileName) {
	const std::vector<tgRod*> rods = subject.getPointer();
	tgRod* const pointer = rods[rods.size() - 1];

	myFile.open(posfilename, ios::app);
	myFile << pointer->centerOfMass().getX() << ", "
			<< pointer->centerOfMass().getY() << ", "
			<< pointer->centerOfMass().getZ() << "\n";

	myFile.close();
	cout << "pos read \n";
}
