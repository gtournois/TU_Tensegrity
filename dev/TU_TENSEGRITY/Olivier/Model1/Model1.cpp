/*
 * Model1.cpp
 *
 *  Created on: Apr 21, 2016
 *      Author: tensegribuntu
 */

// This module
#include "Model1.h"
// This library
#include "core/tgBasicActuator.h"
#include "core/tgSpringCableActuator.h"
#include "core/tgRod.h"
#include "tgcreator/tgBuildSpec.h"
#include "tgcreator/tgBasicActuatorInfo.h"
#include "tgcreator/tgRodInfo.h"
#include "tgcreator/tgStructure.h"
#include "tgcreator/tgStructureInfo.h"
// The Bullet Physics library
#include "LinearMath/btVector3.h"
// The C++ Standard Library
#include <stdexcept>
#include <cmath>

namespace
{
	const struct Config
	{
	      double density;
	      double radius;
	      double stiffness;
	      double damping;
	      double pretension;
	      double stiffness2;
	      double damping2;
	      double pretension2;
	      double triangle_length;
	      double center_distance;
	 } c =
	{
	       0.01,     // density (mass / length^3)
	       0.31,     // radius (length)
	       1000.0,   // stiffness (mass / sec^2)
	       100.0,     // damping (mass / sec)
	       500.0,     // pretension (mass * length / sec^2)
		   3500,
		   100,
		   1000,
	       10.0,     // triangle_height (length)
	       2.5,     // prism_height (length)
	};
}

Model_1::Model_1() :
tgModel()
{
}

Model_1::~Model_1()
{
}

void Model_1::addNodes(tgStructure& s,
						double width,
						double centerDist)
{
	//determine center of tetrahedron
		double heightofcenter=pow(width,2)/(6*sqrt((2*pow(width,2))/3));
		Eigen::Vector3d center, corner, vector, unitvector, offset1, offset2, point6;
		Eigen::Matrix3d R;
		center<<0,heightofcenter,0;
		corner<<(1.0/3.0)*sqrt(3)*width,0,0;
		vector=(center-corner);
		vector(1)=-vector(1);
		unitvector=vector/vector.norm();
		offset1=center+unitvector*centerDist;
		offset2=center-unitvector*centerDist;
		point6=center-unitvector*4*centerDist;

		s.addNode((1.0/3.0)*sqrt(3)*width,0,0);
		s.addNode((-1.0/6.0)*sqrt(3)*width,0,0.5*width);
		s.addNode((-1.0/6.0)*sqrt(3)*width,0,-0.5*width);
		s.addNode(0,(1.0/3.0)*sqrt(6)*width,0);
		s.addNode(offset1(0),offset1(1),offset1(2));
		s.addNode(offset2(0),offset2(1),offset2(2));
		s.addNode(point6(0),point6(1),point6(2));

}

void Model_1::addRods(tgStructure& s)
{
	s.addPair(0, 4, "rod stuck");
	s.addPair(3, 4, "rod stuck");
	s.addPair(2, 5, "rod");
	s.addPair(1, 5, "rod");
	s.addPair(5, 6, "rod");
}

void Model_1::addMuscles(tgStructure& s)
{
	s.addPair(0, 1, "muscle outer1"); //bottom back
	s.addPair(0, 2, "muscle outer2"); //bottom front

	s.addPair(1, 3, "muscle outer3"); //upright back

	s.addPair(2, 3, "muscle outer4"); //upright front

	s.addPair(4, 5, "muscle2 inner");
}

void Model_1::setup(tgWorld& world)
{
    // Define the configurations of the rods and strings
    // Note that pretension is defined for this string
    const tgRod::Config rodConfig(c.radius, c.density);
    const tgRod::Config stuckConfig(c.radius, 0.0);
    const tgBasicActuator::Config muscleConfig(c.stiffness, c.damping, c.pretension);
    const tgBasicActuator::Config muscleConfig2(c.stiffness2, c.damping2, c.pretension2);
    // Create a structure that will hold the details of this model
    tgStructure s;

    // Add nodes to the structure
    addNodes(s, c.triangle_length, c.center_distance);

    // Add rods to the structure
    addRods(s);

    // Add muscles to the structure
    addMuscles(s);

    // Move the structure so it doesn't start in the ground
    s.move(btVector3(0, 20, 0));

    // Create the build spec that uses tags to turn the structure into a real model
    tgBuildSpec spec;
    spec.addBuilder("rod", new tgRodInfo(rodConfig));
    spec.addBuilder("rod stuck", new tgRodInfo(stuckConfig));
    spec.addBuilder("muscle", new tgBasicActuatorInfo(muscleConfig));
    spec.addBuilder("muscle2", new tgBasicActuatorInfo(muscleConfig2));
    // Create your structureInfo
    tgStructureInfo structureInfo(s, spec);

    // Use the structureInfo to build ourselves
    structureInfo.buildInto(*this, world);

    // We could now use tgCast::filter or similar to pull out the
    // models (e.g. muscles) that we want to control.
    allMuscles = tgCast::filter<tgModel, tgBasicActuator> (getDescendants());

    // Notify controllers that setup has finished.
    notifySetup();

    // Actually setup the children
    tgModel::setup(world);
}

void Model_1::step(double dt)
{
    // Precondition
    if (dt <= 0.0)
    {
        throw std::invalid_argument("dt is not positive");
    }
    else
    {
        // Notify observers (controllers) of the step so that they can take action
        notifyStep(dt);
        tgModel::step(dt);  // Step any children
    }
}

void Model_1::onVisit(tgModelVisitor& r)
{
    // Example: m_rod->getRigidBody()->dosomething()...
    tgModel::onVisit(r);
}

const std::vector<tgBasicActuator*>& Model_1::getAllMuscles() const
{
    return allMuscles;
}

void Model_1::teardown()
{
    notifyTeardown();
    tgModel::teardown();
}


