

#ifndef CONTROLLER_2_H
#define CONTROLLER_2_H


// This library
#include "core/tgObserver.h"
#include "learning/Adapters/AnnealAdapter.h"
#include <vector>
#include <ncurses.h>

// Forward declarations
class Model_1;

//namespace std for vectors
using namespace std;


class Controller2 : public tgObserver<Model_1>
{
public:

  // Note that currently this is calibrated for decimeters.
	Controller2(const double prefLength, double timestep);

  virtual ~Controller2() { }

  virtual void onSetup(Model_1& subject);

  virtual void onStep(Model_1& subject, double dt);

private:
  double m_initialLengths;
  double m_totalTime;
  double dt;
  ofstream myfile;

  void setMuscleTargetLength(Model_1& subject, double dt);
  void moveAllMotors(Model_1& subject, double dt);
  void checkButtonPress();
  void getLengths(Model_1& subject, double time);
  void setFromTxt(Model_1& subject);
};

#endif
