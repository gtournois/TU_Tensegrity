/*
 * App1.h
 *
 *  Created on: Apr 21, 2016
 *      Author: Olivier Hiemstra
 */

#include "Model1.h"
#include "Controller2.h"

// This application


// This library
#include "core/terrain/tgBoxGround.h"
#include "core/tgModel.h"
#include "core/tgSimViewGraphics.h"
#include "core/tgSimulation.h"
#include "core/tgWorld.h"
// Bullet Physics
#include "LinearMath/btVector3.h"
// The C++ Standard Library
#include <iostream>

int main(int argc, char** argv)
{
	std::cout << "My First Model" << std::endl;

	//First crate the ground and world.
	 const double yaw = 0.0;
	    const double pitch = 0.0;
	    const double roll = 0.0;
	    const tgBoxGround::Config groundConfig(btVector3(yaw, pitch, roll));
	    // the world will delete this
	    tgBoxGround* ground = new tgBoxGround(groundConfig);

	    const tgWorld::Config config(981); // gravity, cm/sec^2
	    tgWorld world(config, ground);

	    // Second create the view
	    const double timestep_physics = 0.001; // seconds
	    const double timestep_graphics = 1.f/60.f; // seconds
	    tgSimViewGraphics view(world, timestep_physics, timestep_graphics);

	    // Third create the simulation
	    tgSimulation simulation(view);

	    // Fourth create the models with their controllers and add the models to the
	    // simulation
	    Model_1* const myModel = new Model_1();
	    Controller2* const controller = new Controller2(1.5, timestep_physics);

	    myModel->attach(controller);

	    // Add the model to the world
	    simulation.addModel(myModel);

	    simulation.run();

	    //Teardown is handled by delete, so that should be automatic
	    return 0;
}
