

// This module
#include "Controller2.h"
// This application
#include "Model1.h"
// This library
#include "core/tgBasicActuator.h"
// The C++ Standard Library10
#include <cassert>
#include <stdexcept>
#include <vector>
#include <ncurses.h>
#include <iostream>
#include <fstream>



# define M_PI 3.14159265358979323846

using namespace std;

//Constructor using the model subject and a single pref length for all muscles.
Controller2::Controller2(const double initialLength, double timestep) :
    m_initialLengths(initialLength),
    m_totalTime(0.0),
    dt(timestep),
	myfile () {}



//Fetch all the muscles and set their preferred length
void Controller2::onSetup(Model_1& subject) {
	this->m_totalTime=0.0;
	const std::vector<tgBasicActuator*> muscle = subject.find<tgBasicActuator>("muscle");

//	myfile.open("/home/tensegribuntu/NTRTsim/src/dev/TU_TENSEGRITY/Olivier/Model1/muscleLengths.txt");
//		if (myfile.is_open())
//		  {
//			cout << "file created";
//		    myfile << "";
//		    myfile.close();
//		  }
//
//	else cout << "Unable to open file";

    for (size_t i=0; i<muscle.size(); i++) {
		tgBasicActuator * const pMuscle = muscle[i];
		assert(pMuscle != NULL);
	    }
}

// Set target length of each muscle, then move motors accordingly
void Controller2::onStep(Model_1& subject, double dt) {
    // Update controller's internal time
    if (dt <= 0.0) { throw std::invalid_argument("dt is not positive"); }
    m_totalTime+=dt;

//    if (abs(remainder(m_totalTime,3))<0.00005){
//           	getLengths(subject, m_totalTime);
//           	cout<<"get lengths \n";
//    }

    if (m_totalTime>10){
    	setFromTxt(subject);
    	cout<<"lengths set \n";
    }

    moveAllMotors(subject, dt);
}

void Controller2::setMuscleTargetLength(Model_1& subject, double dt) {

	const double mean_muscle_length = 10; //TODO: define according to vars
    double newLength = 0;
    double amplitude1=1 ; //bottom back
    double amplitude2=1 ; //bottom front
    double amplitude3=1 ; //upright back
    double amplitude4=1 ; //upright front
    double amplitude5=1 ;//inner

    double phase1=0 ; //bottom back
    double phase2=0 ; //bottom front
    double phase3=0 ; //upright back
    double phase4=0 ; //upright front
    double phase5=0 ;//inner

    double offset1=0 ; //bottom back
    double offset2=0 ; //bottom front
    double offset3=0 ; //upright back
    double offset4=0 ; //upright front
    double offset5=0 ;//inner

    if (m_totalTime<6){
    	phase1=0;
    	phase2=M_PI;
    	phase3=0;
    	phase4=M_PI;
    }
    if (m_totalTime>6){
    	amplitude1=0;
    	amplitude2=0;
    	amplitude3=-1;
    	amplitude4=-1;
    }
    if(m_totalTime>12){
    	phase2=M_PI/2;
    	phase3=M_PI;
    	phase4=3*M_PI/2;

    }
    const double angular_freq = M_PI/2;

//    const double phase_top_back_left = 1.5*M_PI;
//    const double phase_top_back_right = phase_top_back_left + 1.5*M_PI;
//    const double phase_top_front_right = phase_top_back_left + M_PI;
//    const double phase_top_front_left = phase_top_back_left + 0.5*M_PI;
//    const double phase_bottom_back_left = phase_top_back_left+M_PI;
//    const double phase_bottom_back_right = phase_top_back_left + 0.5*M_PI;
//    const double phase_bottom_front_right = phase_top_back_left;
//    const double phase_bottom_front_left = phase_top_back_left + 1.5*M_PI;

    const double dcOffset = mean_muscle_length;
    const std::vector<tgBasicActuator*> bottom_back = subject.find<tgBasicActuator>("outer1");
    const std::vector<tgBasicActuator*> bottom_front = subject.find<tgBasicActuator>("outer2");
    const std::vector<tgBasicActuator*> upright_back = subject.find<tgBasicActuator>("outer3");
    const std::vector<tgBasicActuator*> upright_front = subject.find<tgBasicActuator>("outer4");
    const std::vector<tgBasicActuator*> inner = subject.find<tgBasicActuator>("inner");


    for (size_t i=0; i<bottom_back.size(); i++) {
        tgBasicActuator * const pMuscle = bottom_back[i];
        assert(pMuscle != NULL);
        newLength = amplitude1 * sin(angular_freq * m_totalTime+phase1) + dcOffset+offset1;
        pMuscle->setControlInput(newLength, dt);
    }
    for (size_t i=0; i<bottom_front.size(); i++) {
           tgBasicActuator * const pMuscle = bottom_front[i];
           assert(pMuscle != NULL);
           newLength = amplitude2 * sin(angular_freq * m_totalTime+phase2) + dcOffset+offset2;
           pMuscle->setControlInput(newLength, dt);
    }
    for (size_t i=0; i<upright_back.size(); i++) {
            tgBasicActuator * const pMuscle = upright_back[i];
            assert(pMuscle != NULL);
            newLength = amplitude3 * sin(angular_freq * m_totalTime+phase3) + dcOffset+offset3;
            pMuscle->setControlInput(newLength, dt);
    }
    for (size_t i=0; i<upright_front.size(); i++) {
            tgBasicActuator * const pMuscle = upright_front[i];
            assert(pMuscle != NULL);
            newLength = amplitude4 * sin(angular_freq * m_totalTime+phase4) + dcOffset+offset4;
            pMuscle->setControlInput(newLength, dt);
    }

    for (size_t i=0; i<inner.size(); i++) {
            tgBasicActuator * const pMuscle = inner[i];
            assert(pMuscle != NULL);
            newLength = amplitude5 * sin(angular_freq * m_totalTime) + dcOffset/5+offset5;
            pMuscle->setControlInput(newLength, dt);
    }
}

//Move motors for all the muscles
void Controller2::moveAllMotors(Model_1& subject, double dt) {
    const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();
    for (size_t i = 0; i < muscles.size(); ++i) {
		tgBasicActuator * const pMuscle = muscles[i];
		assert(pMuscle != NULL);
		pMuscle->moveMotors(dt);
	}

}

void Controller2::checkButtonPress(){
	 initscr();
	 cbreak();
	 noecho();
	 nodelay(stdscr, TRUE);
	 scrollok(stdscr, TRUE);

	 int ch=getch();
	 if (ch != -1) {
		 cout << ch << "\n";
	 //    ungetch(ch);
	 }
}

void Controller2::getLengths(Model_1& subject, double time){

			myfile.open("/home/tensegribuntu/NTRTsim/src/dev/TU_TENSEGRITY/Olivier/Model1/muscleLengths.txt", ios::app);

			const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();
			for (size_t i = 0; i < muscles.size(); ++i) {
					tgBasicActuator * const pMuscle = muscles[i];
					assert(pMuscle != NULL);
					myfile <<pMuscle->getCurrentLength()<< "\n" ;
			}
			myfile.close();
}

void Controller2::setFromTxt(Model_1& subject){
	ifstream in;
	string temp;
	in.open("/home/tensegribuntu/NTRTsim/src/dev/TU_TENSEGRITY/Olivier/Model1/muscleLengths.txt");
	std::vector<double> lengths;

	while ( in.good() ){
		getline (in,temp);
		lengths.push_back(atof(temp.c_str()));
	    }
	myfile.close();

	const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();

	 for (size_t i=0; i<muscles.size(); i++) {
	        tgBasicActuator * const pMuscle = muscles[i];
	        assert(pMuscle != NULL);
	        pMuscle->setControlInput(lengths[i], dt);
	    }

}

