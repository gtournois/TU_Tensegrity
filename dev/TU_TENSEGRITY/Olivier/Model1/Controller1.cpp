#include "Controller1.h"

#include "Model1.h"

#include "core/tgBasicActuator.h"
#include "core/tgSpringCableActuator.h"
#include <cassert>
#include <stdexcept>
#include <vector>
#include <iostream>
#include <cmath>
#include <ncurses.h>
#include <stdio.h>


#define M_PI 3.14159265358979323846

using namespace std;

Controller1::Controller1():
		m_totalTime(0.0) {}

void Controller1::onSetup(Model_1& subject){
	this->m_totalTime=0.0;

	const std::vector<tgBasicActuator*> muscle = subject.getAllMuscles();

	    for (size_t i=0; i<muscle.size(); i++) {
			tgBasicActuator * const pMuscle = muscle[i];
			assert(pMuscle != NULL);
			pMuscle->setControlInput(0.0);
	    }

}

void Controller1::onStep(Model_1& subject, double dt){
	 if (dt <= 0.0) { throw std::invalid_argument("dt is not positive"); }
	 m_totalTime+=dt;

	 setVertical(subject,dt);
	 //setHorizontal(subject,dt);
	 moveMotors(subject,dt);

	 checkInput();
}

void Controller1::setVertical(Model_1& subject, double dt){
//	outer1->setControlInput(3-2*sin(m_totalTime),dt);
//	outer2->setControlInput(3+2*sin(m_totalTime),dt);
//	outer1->setControlInput(3+2*sin(m_totalTime),dt);
//	outer2->setControlInput(3-2*sin(m_totalTime),dt);

	 const std::vector<tgBasicActuator*> muscles = subject.find<tgBasicActuator>("inner");
	 double newLength = 0;
	    for (size_t i=0; i<muscles.size(); i++) {
			tgBasicActuator * const pMuscle = muscles[i];
			assert(pMuscle != NULL);
	        newLength = 1+sin(m_totalTime);
			pMuscle->setControlInput(newLength, dt);
	    }
}

void Controller1::setHorizontal(Model_1& subject, double dt){
//	outer1->setControlInput(1+2*sin(dt),dt);
//	outer2->setControlInput(1+2*sin(dt),dt);
//	outer1->setControlInput(1-2*sin(dt),dt);
//	outer2->setControlInput(1-2*sin(dt),dt);
}

void Controller1::moveMotors(Model_1& subject, double dt){
//	outer1->moveMotors(dt);
//	outer2->moveMotors(dt);
//	outer1->moveMotors(dt);
//	outer2->moveMotors(dt);
	const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();
	    for (size_t i = 0; i < muscles.size(); ++i) {
			tgBasicActuator * const pMuscle = muscles[i];
			assert(pMuscle != NULL);
			pMuscle->moveMotors(dt);
		}
}

void checkInput(){
	char ch;
	ch=getch();
	if (ch==NULL){
		std::cout << "is null" << std::endl;
	}
	std::cout << ch << std::endl;
	std::cout << "working" << std::endl;
}
