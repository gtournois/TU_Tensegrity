/*
 * Model1.h
 *
 *  Created on: Apr 21, 2016
 *      Author: tensegribuntu
 */

#ifndef MODEL1_H
#define MODEL1_H

#include "core/tgModel.h"
#include "core/tgSubject.h"

#include <vector>

//Forward delcarations
class tgSpringCableActuator;
class tgBasicActuator;
class tgRod;
class tgModelVisitor;
class tgStructure;
class tgWorld;
class btRigidBody;
class tgBaseRigid;

class Model_1 : public tgSubject<Model_1>, public tgModel{
public:

	/**
	     * The only constructor. Configuration parameters are within the
	     * .cpp file in this case, not passed in.
	     */
	Model_1();

	/**
	     * Destructor. Deletes controllers, if any were added during setup.
	     * Teardown handles everything else.
	     */
	virtual ~Model_1();

	/**
	     * Create the model. Place the rods and strings into the world
	     * that is passed into the simulation. This is triggered
	     * automatically when the model is added to the simulation, when
	     * tgModel::setup(world) is called (if this model is a child),
	     * and when reset is called. Also notifies controllers of setup.
	     * @param[in] world - the world we're building into
	     */
	virtual void setup(tgWorld& world);

	/**
	     * Undoes setup. Deletes child models. Called automatically on
	     * reset and end of simulation. Notifies controllers of teardown
	     */
	virtual void teardown();

	 /**
	     * Step the model, its children. Notifies controllers of step.
	     * @param[in] dt, the timestep. Must be positive.
	     */
	virtual void step(double dt);

	/**
	     * Receives a tgModelVisitor and dispatches itself into the
	     * visitor's "render" function. This model will go to the default
	     * tgModel function, which does nothing.
	     * @param[in] r - a tgModelVisitor which will pass this model back
	     * to itself
	     */
	virtual void onVisit(tgModelVisitor& r);

	const std::vector<tgBasicActuator*>& getAllMuscles() const;

	const std::vector<tgRod*>& getAllRods() const;

private:

	static void addNodes(tgStructure& s,
							double width,
							double centerDist);

	static void addRods(tgStructure& s);

	static void addMuscles(tgStructure& s);

private:
	std::vector<tgBasicActuator*> allMuscles;
};

#endif /* SRC_DEV_TU_TENSEGRITY_OLIVIER_MODEL1_MODEL1_H_ */
