
#ifndef CONTROLLER_1
#define CONTROLLER_1

// This library
#include "core/tgObserver.h"
#include <vector>

// Forward declarations
class Model_1;
class tgBasicActuator;

//namespace std for vectors
using namespace std;

class Controller1 : public tgObserver<Model_1>
{
public:
	Controller1();

	virtual ~Controller1(){}

	virtual void onSetup(Model_1& subject);

	virtual void onStep(Model_1&, double dt);

private:
	double m_totalTime;

	void setVertical(Model_1& subject, double dt);
	void setHorizontal(Model_1& subject, double dt);
	void moveMotors(Model_1&subject, double dt);
	void checkInput();
};

#endif
