

// This module
#include "FootModel.h"

#include "core/tgBasicActuator.h"
#include "core/tgRod.h"
#include "tgcreator/tgBuildSpec.h"
#include "tgcreator/tgBasicActuatorInfo.h"
#include "tgcreator/tgRodInfo.h"
#include "tgcreator/tgStructure.h"
#include "tgcreator/tgStructureInfo.h"
// The Bullet Physics library
#include "LinearMath/btVector3.h"
// The C++ Standard Library
#include <stdexcept>

/**
 * Anonomous namespace so we don't have to declare the config in
 * the header.
 */
namespace
{
    /**
     * Configuration parameters so they're easily accessable.
     * All parameters must be positive.
     */
    const struct Config
    {
        double density;
        double radius;
        double stiffness;
        double damping;
        double pretension;
        double triangle_length;
        double triangle_height;
        double prism_height;
    } c =
   {
       0.02,     // density (mass / length^3)
       0.31,     // radius (length)
       1000.0,   // stiffness (mass / sec^2)
       10.0,     // damping (mass / sec)
       50.0,     // pretension (mass * length / sec^2)
       10.0,     // triangle_length (length)
       10.0,     // triangle_height (length)
       20.0,     // prism_height (length)
  };
} // namespace

FootModel::FootModel() :
tgModel()
{
}

FootModel::~FootModel()
{
}

void FootModel::addNodes(tgStructure& s)
{

    s.addNode(0, 0, -16);
    s.addNode(7, 10, -4);
    s.addNode(1, 10, -6);
    s.addNode(11, 0, -15);
    s.addNode(2, 0, 0);
    s.addNode(9, 10, -10);
    s.addNode(4, 10, -11);
    s.addNode(10, 0, 0);
    s.addNode(2, 7, -9);
    s.addNode(10, 7, -6.5);
    s.addNode(6, 12, -7.75);
    s.addNode(6, 30, -8);
}

void FootModel::addRods(tgStructure& s)
{
    s.addPair( 0,  1, "rod");
    s.addPair( 2,  3, "rod");
    s.addPair( 4,  5, "rod");
    s.addPair( 6,  7, "rod");
    s.addPair( 8, 10, "rod");
    s.addPair( 9, 10, "rod");
    s.addPair( 10,11, "rod");
}

void FootModel::addMuscles(tgStructure& s)
{
	s.addPair( 0,  2, "muscle");
	s.addPair( 0,  3, "muscle");
	s.addPair( 0,  4, "muscle");
	s.addPair( 1,  2, "muscle");
	s.addPair( 1,  4, "muscle");
	s.addPair( 1,  5, "muscle");
	s.addPair( 2,  6, "muscle");
	s.addPair( 3,  6, "muscle");
	s.addPair( 3,  7, "muscle");
	s.addPair( 4,  7, "muscle");
	s.addPair( 5,  7, "muscle");
	s.addPair( 5,  6, "muscle");
	s.addPair( 8,  2, "muscle");
	s.addPair( 8,  6, "muscle");
	s.addPair( 9,  1, "muscle");
	s.addPair( 9,  5, "muscle");
	s.addPair( 11,  5, "muscle");
	s.addPair( 11,  2, "muscle");
}

void FootModel::setup(tgWorld& world)
{
    // Define the configurations of the rods and strings
    // Note that pretension is defined for this string
    const tgRod::Config rodConfig(c.radius, c.density);
    const tgRod::Config rodConfigMassless(c.radius, 0.00/*c.density*/);
    const tgBasicActuator::Config muscleConfig(c.stiffness, c.damping, c.pretension);

    // Create a structure that will hold the details of this model
    tgStructure s;

    // Add nodes to the structure
    addNodes(s);

    // Add rods to the structure
    addRods(s);

    // Add muscles to the structure
    addMuscles(s);

    // Move the structure so it doesn't start in the ground
    s.move(btVector3(0, 30, 0));

    // Create the build spec that uses tags to turn the structure into a real model
    tgBuildSpec spec;
    spec.addBuilder("rod", new tgRodInfo(rodConfig));
    spec.addBuilder("massless", new tgRodInfo(rodConfigMassless));
    spec.addBuilder("muscle", new tgBasicActuatorInfo(muscleConfig));

    // Create your structureInfo
    tgStructureInfo structureInfo(s, spec);

    // Use the structureInfo to build ourselves
    structureInfo.buildInto(*this, world);

    // We could now use tgCast::filter or similar to pull out the
    // models (e.g. muscles) that we want to control.
    allMuscles = tgCast::filter<tgModel, tgBasicActuator> (getDescendants());

    // Notify controllers that setup has finished.
    notifySetup();
    // Actually setup the children
    tgModel::setup(world);
}

void FootModel::step(double dt)
{
    // Precondition
    if (dt <= 0.0)
    {
        throw std::invalid_argument("dt is not positive");
    }
    else
    {
        // Notify observers (controllers) of the step so that they can take action
        notifyStep(dt);
        tgModel::step(dt);  // Step any children
    }
}

void FootModel::onVisit(tgModelVisitor& r)
{
    // Example: m_rod->getRigidBody()->dosomething()...
    tgModel::onVisit(r);
}

const std::vector<tgBasicActuator*>& FootModel::getAllMuscles() const
{
    return allMuscles;
}

void FootModel::teardown()
{
    notifyTeardown();
    tgModel::teardown();
}
