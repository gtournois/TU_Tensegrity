/*
 * Copyright © 2015, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 * 
 * The NASA Tensegrity Robotics Toolkit (NTRT) v1 platform is licensed
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

/**
 * @file ScarrArmController.cpp
 * @brief Preferred Length Controller for ScarrArmModel
 * @author Steven Lessard
 * @version 1.0.0
 * $Id$
 */

// This module
#include "ScarrArmController.h"
// This application
#include "ScarrArmModel.h"
// This library
#include "core/tgBasicActuator.h"
// The C++ Standard Library
#include <cassert>
#include <stdexcept>
#include <vector>

# define M_PI 3.14159265358979323846 

using namespace std;

//Constructor using the model subject and a single pref length for all muscles.
ScarrArmController::ScarrArmController(const double initialLength, double timestep) :
    m_initialLengths(initialLength),
    m_totalTime(0.0),
    dt(timestep) {}

//Fetch all the muscles and set their preferred length
void ScarrArmController::onSetup(ScarrArmModel& subject) {
	this->m_totalTime=0.0;
	const double radius_circle=2;
    const double muscle_length = 2*sqrt(radius_circle);

	const std::vector<tgBasicActuator*> muscle = subject.find<tgBasicActuator>("muscle");


    for (size_t i=0; i<muscle.size(); i++) {
		tgBasicActuator * const pMuscle = muscle[i];
		assert(pMuscle != NULL);
		pMuscle->setControlInput(muscle_length, dt);
    }
     
}

// Set target length of each muscle, then move motors accordingly
void ScarrArmController::onStep(ScarrArmModel& subject, double dt) {
    // Update controller's internal time
    if (dt <= 0.0) { throw std::invalid_argument("dt is not positive"); }
    m_totalTime+=dt;
    std::cout << m_totalTime << std::endl;
    setMuscleTargetLength(subject, dt);
    moveAllMotors(subject, dt);
    //updateActions(dt);
}
 
void ScarrArmController::setMuscleTargetLength(ScarrArmModel& subject, double dt) {
    const double radius_circle=3;
	const double mean_muscle_length = 2*sqrt(radius_circle); //TODO: define according to vars
    double newLength = 0;
    const double amplitude = radius_circle;
    const double angular_freq = M_PI;

    const double phase_top_back_left = 1.5*M_PI;
    const double phase_top_back_right = phase_top_back_left + 1.5*M_PI;
    const double phase_top_front_right = phase_top_back_left + M_PI;
    const double phase_top_front_left = phase_top_back_left + 0.5*M_PI;
    const double phase_bottom_back_left = phase_top_back_left+M_PI;
    const double phase_bottom_back_right = phase_top_back_left + 0.5*M_PI;
    const double phase_bottom_front_right = phase_top_back_left;
    const double phase_bottom_front_left = phase_top_back_left + 1.5*M_PI;

    const double dcOffset = mean_muscle_length;
    const std::vector<tgBasicActuator*> top_back_left = subject.find<tgBasicActuator>("top_back_left muscle");
    const std::vector<tgBasicActuator*> top_back_right = subject.find<tgBasicActuator>("top_back_right muscle");
    const std::vector<tgBasicActuator*> top_front_right = subject.find<tgBasicActuator>("top_front_right muscle");
    const std::vector<tgBasicActuator*> top_front_left = subject.find<tgBasicActuator>("top_front_left muscle");
    const std::vector<tgBasicActuator*> bottom_back_left = subject.find<tgBasicActuator>("bottom_back_left muscle");
    const std::vector<tgBasicActuator*> bottom_back_right = subject.find<tgBasicActuator>("bottom_back_right muscle");
    const std::vector<tgBasicActuator*> bottom_front_right = subject.find<tgBasicActuator>("bottom_front_right muscle");
    const std::vector<tgBasicActuator*> bottom_front_left = subject.find<tgBasicActuator>("bottom_front_left muscle");

    for (size_t i=0; i<top_back_left.size(); i++) {
        tgBasicActuator * const pMuscle = top_back_left[i];
        assert(pMuscle != NULL);
        newLength = amplitude * sin(angular_freq * m_totalTime + phase_top_back_left) + dcOffset;
        pMuscle->setControlInput(newLength, dt);
    }
    for (size_t i=0; i<top_back_right.size(); i++) {
           tgBasicActuator * const pMuscle = top_back_right[i];
           assert(pMuscle != NULL);
           newLength = amplitude * sin(angular_freq * m_totalTime + phase_top_back_right) + dcOffset;
           pMuscle->setControlInput(newLength, dt);
    }
    for (size_t i=0; i<top_front_right.size(); i++) {
            tgBasicActuator * const pMuscle = top_front_right[i];
            assert(pMuscle != NULL);
            newLength = amplitude * sin(angular_freq * m_totalTime + phase_top_front_right) + dcOffset;
            pMuscle->setControlInput(newLength, dt);
    }
    for (size_t i=0; i<top_front_left.size(); i++) {
            tgBasicActuator * const pMuscle = top_front_left[i];
            assert(pMuscle != NULL);
            newLength = amplitude * sin(angular_freq * m_totalTime + phase_top_front_left) + dcOffset;
            pMuscle->setControlInput(newLength, dt);
    }

    for (size_t i=0; i<bottom_back_left.size(); i++) {
            tgBasicActuator * const pMuscle = bottom_back_left[i];
            assert(pMuscle != NULL);
            newLength = amplitude * sin(angular_freq * m_totalTime + phase_bottom_back_left) + dcOffset;
            pMuscle->setControlInput(newLength, dt);
    }
    for (size_t i=0; i<bottom_back_right.size(); i++) {
               tgBasicActuator * const pMuscle = bottom_back_right[i];
               assert(pMuscle != NULL);
               newLength = amplitude * sin(angular_freq * m_totalTime + phase_bottom_back_right) + dcOffset;
               pMuscle->setControlInput(newLength, dt);
    }
    for (size_t i=0; i<bottom_front_right.size(); i++) {
               tgBasicActuator * const pMuscle = bottom_front_right[i];
               assert(pMuscle != NULL);
               newLength = amplitude * sin(angular_freq * m_totalTime + phase_bottom_front_right) + dcOffset;
               pMuscle->setControlInput(newLength, dt);
    }
    for (size_t i=0; i<bottom_front_left.size(); i++) {
               tgBasicActuator * const pMuscle = bottom_front_left[i];
               assert(pMuscle != NULL);
               newLength = amplitude * sin(angular_freq * m_totalTime + phase_bottom_front_left) + dcOffset;
               pMuscle->setControlInput(newLength, dt);
   }




}

//Move motors for all the muscles
void ScarrArmController::moveAllMotors(ScarrArmModel& subject, double dt) {
    const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();
    for (size_t i = 0; i < muscles.size(); ++i) {
		tgBasicActuator * const pMuscle = muscles[i];
		assert(pMuscle != NULL);
		pMuscle->moveMotors(dt);
	}
     
}

// Get actions from evolutionAdapter, transform them to this structure, and apply them
void ScarrArmController::updateActions(ScarrArmModel& subject, double dt) {
	/*vector<double> state=getState();
	vector< vector<double> > actions;

	//get the actions (between 0 and 1) from evolution (todo)
	actions=evolutionAdapter.step(dt,state);

	//transform them to the size of the structure
	actions = transformActions(actions);

	//apply these actions to the appropriate muscles according to the sensor values
    applyActions(subject,actions);
    */
}

//Scale actions according to Min and Max length of muscles.
vector< vector <double> > ScarrArmController::transformActions(vector< vector <double> > actions)
{
	double min=2*sqrt(2)-2;
	double max=2*sqrt(2)+4;
	double range=max-min;
	double scaledAct;
	for(unsigned i=0;i<actions.size();i++) {
		for(unsigned j=0;j<actions[i].size();j++) {
			scaledAct=actions[i][j]*(range)+min;
			actions[i][j]=scaledAct;
		}
	}
	return actions;
}

//Pick particular muscles (according to the structure's state) and apply the given actions one by one
void ScarrArmController::applyActions(ScarrArmModel& subject, vector< vector <double> > act)
{
	//Get All the muscles of the subject
	const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();
	//Check if the number of the actions match the number of the muscles
	if(act.size() != muscles.size()) {
		cout<<"Warning: # of muscles: "<< muscles.size() << " != # of actions: "<< act.size()<<endl;
		return;
	}
	//Apply actions (currently in a random order)
	for (size_t i = 0; i < muscles.size(); ++i)	{
		tgBasicActuator * const pMuscle = muscles[i];
		assert(pMuscle != NULL);
		//cout<<"i: "<<i<<" length: "<<act[i][0]<<endl;
		pMuscle->setControlInput(act[i][0]);
	}
}
