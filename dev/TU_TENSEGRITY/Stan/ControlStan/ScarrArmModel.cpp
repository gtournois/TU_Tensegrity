/*
 * Copyright © 2015, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 * 
 * The NASA Tensegrity Robotics Toolkit (NTRT) v1 platform is licensed
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
*/

/**
 * @file ScarrArmModel.cpp
 * @brief Contains the implementation of class ScarrArmModel. See README
 * $Id$
 */

// This module
#include "ScarrArmModel.h"
// This library
#include "core/tgBasicActuator.h"
#include "core/tgRod.h"
#include "core/abstractMarker.h"
#include "tgcreator/tgBuildSpec.h"
#include "tgcreator/tgBasicActuatorInfo.h"
#include "tgcreator/tgKinematicContactCableInfo.h"
#include "tgcreator/tgRodInfo.h"
#include "tgcreator/tgStructure.h"
#include "tgcreator/tgStructureInfo.h"
// The Bullet Physics library
#include "LinearMath/btVector3.h"
// The C++ Standard Library
#include <stdexcept>

namespace {
    // see tgBaseString.h for a descripton of some of these rod parameters
    // (specifically, those related to the motor moving the strings.)
    // NOTE that any parameter that depends on units of length will scale
    // with the current gravity scaling. E.g., with gravity as 98.1,
    // the length units below are in decimeters.

    const struct ConfigRod {
        double density;
        double radius;
        double friction;
        double rollFriction;
        double restitution;
    } cRod = {
        0.2,    // density (kg / length^3)
        0.3,     // radius (length)
        1.0,      // friction (unitless)
        0.01,     // rollFriction (unitless)
        0.2      // restitution (?)
    };

    const struct ConfigCable {
        double elasticity;
        double damping;
        double stiffness;
        double pretension;
        bool   history;  
        double maxTens;
        double targetVelocity; 
        double mRad;
        double motorFriction;
        double motorInertia;
        bool backDrivable;
    } cCable = {
        1000.0,         // elasticity
        10,          // damping (kg/s)
        1000.0,         // stiffness (kg / sec^2)
        500.0,       // pretension
        false,          // history (boolean)
        10000,         // maxTens
        1000,           // targetVelocity
        1.0,            // mRad
        10.0,           // motorFriction
        1.0,            // motorInertia
        false           // backDrivable (boolean)
    };
} // namespace

ScarrArmModel::ScarrArmModel() : tgModel() {}

ScarrArmModel::~ScarrArmModel() {}

void ScarrArmModel::addNodes(tgStructure& s) {
    const double rod_length=12;
    const double radius_circle=rod_length/6;
    
    //Top back left
    s.addNode(-2*radius_circle,rod_length,2*radius_circle);//0
    //bottom back left
    s.addNode(-2*radius_circle,0,2*radius_circle);//1
    //Top back right
    s.addNode(2*radius_circle,rod_length,2*radius_circle);//2
    //bottom back left
    s.addNode(2*radius_circle,0,2*radius_circle);//3
    //Top front left
    s.addNode(-2*radius_circle,rod_length,-2*radius_circle);//4
    //bottom front left
    s.addNode(-2*radius_circle,0,-2*radius_circle);//5
    //Top front right
    s.addNode(2*radius_circle,rod_length,-2*radius_circle);//6
    //bottom front right
    s.addNode(2*radius_circle,0,-2*radius_circle);//7
    //Middle top
    s.addNode(0,rod_length,0);//8
    //Middle bottom
    s.addNode(0,0,0);//9
    }
                  
void ScarrArmModel::addRods(tgStructure& s) {   
    s.addPair(0,  1,  "massless");
    s.addPair(2,  3,  "massless");
    s.addPair(4,  5,  "massless");
    s.addPair(6,  7,  "massless");

    s.addPair(8,  9,  "rod");
}

void ScarrArmModel::addMuscles(tgStructure& s) {
    const std::vector<tgStructure*> children = s.getChildren();

    s.addPair(0, 8, "top_back_left muscle");
    s.addPair(2, 8, "top_back_right muscle");
    s.addPair(4, 8, "top_front_left muscle");
    s.addPair(6, 8, "top_front_right muscle");
    s.addPair(1, 9, "bottom_back_left muscle");
    s.addPair(3, 9, "bottom_back_right muscle");
    s.addPair(5, 9, "bottom_front_left muscle");
    s.addPair(7, 9, "bottom_front_right muscle");
    }
 
/*
void ScarrArmModel::addMarkers(tgStructure &s) {
    std::vector<tgRod *> rods=find<tgRod>("rod");

	for(int i=0;i<10;i++)
	{
		const btRigidBody* bt = rods[rodNumbersPerNode[i]]->getPRigidBody();
		btTransform inverseTransform = bt->getWorldTransform().inverse();
		btVector3 pos = inverseTransform * (nodePositions[i]);
		abstractMarker tmp=abstractMarker(bt,pos,btVector3(0.08*i,1.0 - 0.08*i,.0),i);
		this->addMarker(tmp);
	}
}
*/
 
void ScarrArmModel::setup(tgWorld& world) {
    const tgRod::Config rodConfig(cRod.radius, cRod.density, cRod.friction, cRod.rollFriction, cRod.restitution);
    const tgRod::Config rodConfigMassless(cRod.radius, 0.00/*c.density*/, cRod.friction, cRod.rollFriction, cRod.restitution);
    /// @todo acceleration constraint was removed on 12/10/14 Replace with tgKinematicActuator as appropriate

    tgBasicActuator::Config MuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension,
                                                  cCable.history, cCable.maxTens, cCable.targetVelocity);

            
    // Start creating the structure
    tgStructure s;
    addNodes(s);
    addRods(s);
    addMuscles(s);
    
    // Move the arm out of the ground
    btVector3 offset(0.0, 5.0, 0.0);
    s.move(offset);
    
    // Create the build spec that uses tags to turn the structure into a real model
    tgBuildSpec spec;
    spec.addBuilder("massless", new tgRodInfo(rodConfigMassless));
    spec.addBuilder("rod", new tgRodInfo(rodConfig));
    spec.addBuilder("muscle", new tgBasicActuatorInfo(MuscleConfig));

    
    // Create your structureInfo
    tgStructureInfo structureInfo(s, spec);

    // Use the structureInfo to build ourselves
    structureInfo.buildInto(*this, world);

    // We could now use tgCast::filter or similar to pull out the
    // models (e.g. muscles) that we want to control. 
    allMuscles = tgCast::filter<tgModel, tgBasicActuator> (getDescendants());

    // call the onSetup methods of all observed things e.g. controllers
    notifySetup();

    // Actually setup the children
    tgModel::setup(world);

    //map the rods and add the markers to them
    //addMarkers(s);

}

void ScarrArmModel::step(double dt)
{
    // Precondition
    if (dt <= 0.0) {
        throw std::invalid_argument("dt is not positive");
    } else {
        // Notify observers (controllers) of the step so that they can take action
        notifyStep(dt);
        tgModel::step(dt);  // Step any children
    }
}

void ScarrArmModel::onVisit(tgModelVisitor& r)
{
    tgModel::onVisit(r);
}

const std::vector<tgBasicActuator*>& ScarrArmModel::getAllMuscles() const
{
    return allMuscles;
}
    
void ScarrArmModel::teardown()
{
    notifyTeardown();
    tgModel::teardown();
}

