/*
 * Copyright © 2015, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 * 
 * The NASA Tensegrity Robotics Toolkit (NTRT) v1 platform is licensed
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
*/

/**
 * @file LegModel.cpp
 * @brief Contains the implementation of class LegModel. See README
 * $Id$
 */

// This module
#include "LegModel.h"
// This library
#include "core/tgBasicActuator.h"
#include "core/tgRod.h"
#include "core/abstractMarker.h"
#include "tgcreator/tgBuildSpec.h"
#include "tgcreator/tgBasicActuatorInfo.h"
#include "tgcreator/tgKinematicContactCableInfo.h"
#include "tgcreator/tgRodInfo.h"
#include "tgcreator/tgStructure.h"
#include "tgcreator/tgStructureInfo.h"
// The Bullet Physics library
#include "LinearMath/btVector3.h"
// The C++ Standard Library
#include <stdexcept>

# define M_PI 3.1415926535897932384

namespace {
    // see tgBaseString.h for a descripton of some of these rod parameters
    // (specifically, those related to the motor moving the strings.)
    // NOTE that any parameter that depends on units of length will scale
    // with the current gravity scaling. E.g., with gravity as 98.1,
    // the length units below are in decimeters.

    const struct ConfigRod {
        double density;
        double radius;
        double friction;
        double rollFriction;
        double restitution;
    } cRod = {
        1.19,    // density of PMMA(kg / length^3)
        0.03657,     // radius (length)
        1.0,      // friction (unitless)
        0.01,     // rollFriction (unitless)
        0.2      // restitution (?)
    };
    const struct ConfigStructure {
    	  double upper_offset;
    	  double lower_offset;
    	  double upper_length;
    	  double lower_length;
    	  double upper_tip_length;
    	  double lower_tip_length;
    	  double lower_z_offset;
    	  double scaling_pelvis;
    	  double scaling_foot;

    } cStructure= {

    		6.1,
			0.7,
			3.7,
			4.4,
			1,
			1,
			0,
			3.4/20,
			.13

    };
    const struct ConfigLigament {

        double damping;
        double stiffness;
        double pretension_1;
        double pretension_2;
        double pretension_3;
        double pretension_4;
        bool   history;  
        double maxTens;
        double targetVelocity; 
        double mRad;
        double motorFriction;
        double motorInertia;
        bool backDrivable;
    } cLigament = {

        60.0,         	 // damping (kg/s)
		100.0,         // stiffness (kg / sec^2)
        20,      	 // pretension_1
		20,      	 // pretension_2
		20,      	 // pretension_3
		20,      	 // pretension_4
	    false,         	 // history (boolean)
        80000,        	 // maxTens
        10000,           // targetVelocity  
        1.0,           	 // mRad
        10.0,          	 // motorFriction
        1.0,           	 // motorInertia
        false          	 // backDrivable (boolean)
    };
    const struct ConfigPelvis {

           double damping;
           double stiffness;
           double pretension_1;
           double pretension_calf;
           bool   history;
           double maxTens;
           double targetVelocity;
           double mRad;
           double motorFriction;
           double motorInertia;
           bool backDrivable;
       } cPelvis = {

           60.0,         	 // damping (kg/s)
           100.0,         // stiffness (kg / sec^2)
           20,      	 // pretension_1
		   100,				// pretension calf
           false,         	 // history (boolean)
           80000,        	 // maxTens
           10000,           // targetVelocity
           1.0,           	 // mRad
           10.0,          	 // motorFriction
           1.0,           	 // motorInertia
           false          	 // backDrivable (boolean)
       };
} // namespace

LegModel::LegModel() : tgModel() {}

LegModel::~LegModel() {}

/*

pair_groups:
  superball_rod:
    # coronal plane
    - [left_vert_bottom,left_vert_top]
    - [right_vert_bottom,right_vert_top]
    # sagital plane
    - [bottom_horiz_prox_left,bottom_horiz_dist_left]
    - [top_horiz_dist_left,top_horiz_prox_left]
    - [bottom_horiz_prox_right,bottom_horiz_dist_right]
    - [top_horiz_dist_right,top_horiz_prox_right]
    # trasverse plane
    - [dist_horiz_left,dist_horiz_right]
    - [prox_horiz_left,prox_horiz_right]

  middle_rod:
    - [middle_vert_bottom,middle_vert_top]

  pelvis_string:
    - [left_vert_bottom,bottom_horiz_prox_left]
    - [left_vert_bottom,bottom_horiz_dist_left]
    - [left_vert_bottom,dist_horiz_left]
    - [left_vert_bottom,prox_horiz_left]

    - [left_vert_top,top_horiz_dist_left]
    - [left_vert_top,top_horiz_prox_left]
    - [left_vert_top,dist_horiz_left]
    - [left_vert_top,prox_horiz_left]

    - [right_vert_bottom,bottom_horiz_prox_right]
    - [right_vert_bottom,bottom_horiz_dist_right]
    - [right_vert_bottom,dist_horiz_right]
    - [right_vert_bottom,prox_horiz_right]

    - [right_vert_top,top_horiz_prox_right]
    - [right_vert_top,top_horiz_dist_right]
    - [right_vert_top,dist_horiz_right]
    - [right_vert_top,prox_horiz_right]

    - [bottom_horiz_prox_left,prox_horiz_left]
    - [bottom_horiz_prox_right,prox_horiz_right]
    - [bottom_horiz_dist_left,dist_horiz_left]
    - [bottom_horiz_dist_right,dist_horiz_right]

    - [top_horiz_prox_left,prox_horiz_left]
    - [top_horiz_prox_right,prox_horiz_right]
    - [top_horiz_dist_left,dist_horiz_left]
    - [top_horiz_dist_right,dist_horiz_right]

  middle_horiz_string:
    - [top_horiz_prox_left,middle_vert_top]
    - [top_horiz_dist_left,middle_vert_top]
    - [top_horiz_prox_right,middle_vert_top]
    - [top_horiz_dist_right,middle_vert_top]
    - [bottom_horiz_prox_left,middle_vert_bottom]
    - [bottom_horiz_dist_left,middle_vert_bottom]
    - [bottom_horiz_prox_right,middle_vert_bottom]
    - [bottom_horiz_dist_right,middle_vert_bottom]

*/

void LegModel::addNodes(tgStructure& s) {
	const double x=-7.5*cStructure.scaling_pelvis;
	const double y=cStructure.upper_offset+cStructure.upper_length-30*cStructure.scaling_pelvis;
	const double sc=cStructure.scaling_pelvis;
	const double sf=cStructure.scaling_foot;
	const double zf=-7*sf;
	const double xf=6*sf;
	const double yf=cStructure.lower_offset-14*sf;

		//Pelvis
 		//middle vertical rod
		s.addNode(x, 15*sc+y,0);//0
		s.addNode(x, 30*sc+y,0);//1

		//right vertical rod
		s.addNode(x-7.5*sc,15*sc+y,0);//2
		s.addNode(x-7.5*sc,30*sc+y,0);//3

		//bottom horizontal rod left
		s.addNode(x+6*sc, 20*sc+y,-6*sc);//4
		s.addNode(x+6*sc, 20*sc+y,6*sc);//5

		//top horizontal rod left
		s.addNode(x+6*sc, 27.5*sc+y,-6*sc);//6
		s.addNode(x+6*sc, 27.5*sc+y,6*sc);//7

		//bottom horizontal rod right
		s.addNode(x-6*sc, 20*sc+y,-6*sc);//8
		s.addNode(x-6*sc, 20*sc+y,6*sc);//9

		//top horizontal rod right
		s.addNode(x-6*sc, 27.5*sc+y,-6*sc);//10
		s.addNode(x-6*sc, 27.5*sc+y,6*sc);//11

		//proxal horizontal rod
		s.addNode(x-10*sc, 23.75*sc+y,-3*sc);//12
		s.addNode(x+10*sc, 23.75*sc+y,-3*sc);//13

		//distal horizontal rod
		s.addNode(x-10*sc, 23.75*sc+y,3*sc);//14
		s.addNode(x+10*sc, 23.75*sc+y,3*sc);//15



		//Knee

        //upper bones knee
	    s.addNode(0,cStructure.upper_offset+cStructure.upper_length,0);//16
	    s.addNode(0,cStructure.upper_offset, 0); //17
	    s.addNode(0,cStructure.upper_offset-(cStructure.upper_tip_length/sqrt(2)),cStructure.upper_tip_length/sqrt(2)); //18
	    s.addNode((cStructure.upper_tip_length/2)*sqrt(3)/sqrt(2),cStructure.upper_offset-(cStructure.upper_tip_length/sqrt(2)),-(cStructure.upper_tip_length/2/sqrt(2)));//19
	    s.addNode(-(cStructure.upper_tip_length/2)*sqrt(3)/sqrt(2),cStructure.upper_offset-(cStructure.upper_tip_length/sqrt(2)),-(cStructure.upper_tip_length/2/sqrt(2)));//20

	    //Lower bones knee
	    s.addNode(0,cStructure.lower_offset,cStructure.lower_z_offset);//21
	    s.addNode(0,cStructure.lower_offset+cStructure.lower_length,cStructure.lower_z_offset);//22
	    s.addNode(-cStructure.lower_tip_length/sqrt(2),cStructure.lower_offset+cStructure.lower_length+cStructure.lower_tip_length/sqrt(2),cStructure.lower_z_offset);//23
	    s.addNode(cStructure.lower_tip_length/sqrt(2),cStructure.lower_offset+cStructure.lower_length+cStructure.lower_tip_length/sqrt(2),cStructure.lower_z_offset);//24

	    //Ankle
	    s.addNode(-cStructure.lower_tip_length/sqrt(2)+.1,cStructure.lower_offset-cStructure.lower_tip_length/sqrt(2),cStructure.lower_z_offset-0.2);//25
	  	s.addNode(cStructure.lower_tip_length/sqrt(2)-.1,cStructure.lower_offset-cStructure.lower_tip_length/sqrt(2),cStructure.lower_z_offset+0.2);//26

	  	//Foot
	  	s.addNode(-0+xf, 0+yf, 16*sf+zf); //27
	  	s.addNode(-7*sf+xf, 10*sf+yf, 4*sf+zf); //28
	  	s.addNode(-1*sf+xf, 10*sf+yf, 6*sf+zf); //29
	  	s.addNode(-11*sf+xf, 0+yf, 15*sf+zf); //30
	  	s.addNode(-2*sf+xf, 0+yf, 0+zf); //31
	  	s.addNode(-9*sf+xf, 10*sf+yf, 10*sf+zf); //32
	  	s.addNode(-4*sf+xf, 10*sf+yf, 11*sf+zf); //33
	  	s.addNode(-10*sf+xf, 0+yf, 0+zf); //34

}


                  
void LegModel::addRods(tgStructure& s) {

	//left vertical rod
	s.addPair(16,  17,  "bone massless");
	//middle vertical rod
	s.addPair(0, 1, "hip massless bone");
	//right vertical rod
	s.addPair(2, 3, "hip massless bone");
	//bottom horizontal rod left
	s.addPair(4, 5, "hip bone");
	s.addPair(6, 7, "hip bone");
	s.addPair(8, 9, "hip bone");
	s.addPair(10, 11, "hip bone");
	s.addPair(12, 13, "hip bone");
	s.addPair(14, 15, "hip bone");


    // upper knee bones

    s.addPair(17,  18,  "bone massless");
    s.addPair(17,  19,  "bone massless");
    s.addPair(17,  20,  "bone massless");

    // lower knee bones
    s.addPair(21,  22,  "bone");
    s.addPair(22,  23,  "bone");
    s.addPair(22,  24,  "bone");

    // ankle
    s.addPair(21,  25,  "bone");
    s.addPair(21,  26,  "bone");

    // foot
    s.addPair( 27,  28, "bone");
    s.addPair( 29,  30, "bone");
    s.addPair( 31,  32, "bone");
    s.addPair( 33,  34, "bone");

}

 void LegModel::addMuscles(tgStructure& s) {
    const std::vector<tgStructure*> children = s.getChildren();

    //Pelvis Muscles

    //upper half
    s.addPair(16, 6, "pelvis");
    s.addPair(16, 7, "pelvis");
    s.addPair(16, 13, "pelvis");
    s.addPair(16, 15, "pelvis");
    s.addPair(1, 6, "pelvis");
    s.addPair(1, 7, "pelvis");
    s.addPair(1, 10, "pelvis");
    s.addPair(1, 11, "pelvis");
    s.addPair(3, 10, "pelvis");
    s.addPair(3, 11, "pelvis");
    s.addPair(3, 12, "pelvis");
    s.addPair(3, 14, "pelvis");
    s.addPair(6, 13, "pelvis");
    s.addPair(7, 15, "pelvis");
    s.addPair(10, 12, "pelvis");
    s.addPair(11, 14, "pelvis");

    //lower half
    s.addPair(0, 4, "pelvis");
    s.addPair(0, 5, "pelvis");
    s.addPair(0, 8, "pelvis");
    s.addPair(0, 9, "pelvis");
    s.addPair(2, 8, "pelvis");
    s.addPair(2, 9, "pelvis");
    s.addPair(2, 12, "pelvis");
    s.addPair(2, 14, "pelvis");
    s.addPair(4, 13, "pelvis");
    s.addPair(5, 15, "pelvis");
    s.addPair(8, 12, "pelvis");
    s.addPair(9, 14, "pelvis");

    //pelvis to knee muscles
    s.addPair(16, 19, "pelvis");
    s.addPair(16, 24, "pelvis");
    s.addPair(15, 18, "pelvis");
    s.addPair(4, 20, "pelvis");
    s.addPair(5, 23, "pelvis");


    //knee ligaments
    s.addPair(18, 24, "Knee ligament_1");
    s.addPair(18, 23, "Knee ligament_2");
    s.addPair(19, 24, "Knee ligament_3");
    s.addPair(20, 23, "Knee ligament_4");

    //knee to foot
    s.addPair(18, 33, "pelvis");
    s.addPair(19, 29, "calf");
    s.addPair(20, 28, "calf");
    s.addPair(18, 32, "pelvis");
   // s.addPair()

    //Foot muscles
    s.addPair( 27,  29, "Foot muscle");
    s.addPair( 27,  30, "Foot muscle");
    s.addPair( 27,  31, "Foot muscle");
    s.addPair( 28,  29, "Foot muscle");
    s.addPair( 28,  31, "Foot muscle");
    s.addPair( 28,  32, "Foot muscle");
    s.addPair( 29,  33, "Foot muscle");
    s.addPair( 30,  33, "Foot muscle");
    s.addPair( 30,  34, "Foot muscle");
    s.addPair( 31,  34, "Foot muscle");
    s.addPair( 32,  34, "Foot muscle");
    s.addPair( 32,  33, "Foot muscle");
    s.addPair( 25,  28, "Foot muscle");
    s.addPair( 25,  32, "Foot muscle");
    s.addPair( 26,  33, "Foot muscle");
    s.addPair( 26,  29, "Foot muscle");
//    s.addPair( 26,  32, "Foot muscle");
//    s.addPair( 26,  29, "Foot muscle");




}
 
/*
void LegModel::addMarkers(tgStructure &s) {
    std::vector<tgRod *> rods=find<tgRod>("rod");

	for(int i=0;i<10;i++)
	{
		const btRigidBody* bt = rods[rodNumbersPerNode[i]]->getPRigidBody();
		btTransform inverseTransform = bt->getWorldTransform().inverse();
		btVector3 pos = inverseTransform * (nodePositions[i]);
		abstractMarker tmp=abstractMarker(bt,pos,btVector3(0.08*i,1.0 - 0.08*i,.0),i);
		this->addMarker(tmp);
	}
}
*/
 
void LegModel::setup(tgWorld& world) {
    const tgRod::Config boneConfig(cRod.radius, cRod.density, cRod.friction, cRod.rollFriction, cRod.restitution);
    const tgRod::Config boneMasslessConfig(cRod.radius, 0, cRod.friction, cRod.rollFriction, cRod.restitution);
    const tgRod::Config hipBoneConfig(cRod.radius, cRod.density, cRod.friction, cRod.rollFriction, cRod.restitution);
    /// @todo acceleration constraint was removed on 12/10/14 Replace with tgKinematicActuator as appropreate
    tgBasicActuator::Config PelvisMuscleConfig(cPelvis.stiffness, cPelvis.damping, cPelvis.pretension_1,
                                                     cPelvis.history, cPelvis.maxTens, cPelvis.targetVelocity);
    tgBasicActuator::Config calfMuscleConfig(cPelvis.stiffness, cPelvis.damping, cPelvis.pretension_calf,
                                                        cPelvis.history, cPelvis.maxTens, cPelvis.targetVelocity);
    tgBasicActuator::Config KneeLigament1Config(cLigament.stiffness, cLigament.damping, cLigament.pretension_1,
                                                  cLigament.history, cLigament.maxTens, cLigament.targetVelocity);
    tgBasicActuator::Config KneeLigament2Config(cLigament.stiffness, cLigament.damping, cLigament.pretension_2,
                                                  cLigament.history, cLigament.maxTens, cLigament.targetVelocity);
    tgBasicActuator::Config KneeLigament3Config(cLigament.stiffness, cLigament.damping, cLigament.pretension_3,
                                                  cLigament.history, cLigament.maxTens, cLigament.targetVelocity);
    tgBasicActuator::Config KneeLigament4Config(cLigament.stiffness, cLigament.damping, cLigament.pretension_4,
                                                  cLigament.history, cLigament.maxTens, cLigament.targetVelocity);
    tgBasicActuator::Config FootMuscleConfig(cLigament.stiffness, cLigament.damping, cLigament.pretension_4,
                                                      cLigament.history, cLigament.maxTens, cLigament.targetVelocity);
   /* tgBasicActuator::Config rightMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_right,
                                                 cCable.history, cCable.maxTens, cCable.targetVelocity);
    tgBasicActuator::Config leftMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_left,
                                                        cCable.history, cCable.maxTens, cCable.targetVelocity);
    tgBasicActuator::Config topMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_top,
                                                      cCable.history, cCable.maxTens, cCable.targetVelocity);
    tgBasicActuator::Config verticalMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_vertical,
                                                      cCable.history, cCable.maxTens, cCable.targetVelocity);
    tgBasicActuator::Config horizontalMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_horizontal,
                                                      cCable.history, cCable.maxTens, cCable.targetVelocity);
   // tgBasicActuator::Config bottomMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_bottom,
   //                                                     cCable.history, cCable.maxTens, cCable.targetVelocity); // this acts on all the muscles that touch the ground at the start
            */
    // Start creating the structure
    tgStructure s;
    addNodes(s);
    addRods(s);
    addMuscles(s);
    
    // rotate the structure 180*
    	btVector3 rotationPoint = btVector3(0, 0, 0); // origin
    	btVector3 rotationAxis = btVector3(0, 1, 0);  // y-axis
    	double rotationAngle = M_PI;
    	s.addRotation(rotationPoint, rotationAxis, rotationAngle);

    // Move the arm out of the ground
    btVector3 offset(0.0, 2, 0.0);
    s.move(offset);
    

    // Create the build spec that uses tags to turn the structure into a real model
    tgBuildSpec spec;
    spec.addBuilder("bone", new tgRodInfo(boneConfig));
    spec.addBuilder("hip", new tgRodInfo(hipBoneConfig));
    spec.addBuilder("massless", new tgRodInfo(boneMasslessConfig));


    spec.addBuilder("pelvis", new tgBasicActuatorInfo(PelvisMuscleConfig));
    spec.addBuilder("calf", new tgBasicActuatorInfo(calfMuscleConfig));spec.addBuilder("Knee ligament_1", new tgBasicActuatorInfo(KneeLigament1Config));
    spec.addBuilder("Knee ligament_2", new tgBasicActuatorInfo(KneeLigament2Config));
    spec.addBuilder("Knee ligament_3", new tgBasicActuatorInfo(KneeLigament3Config));
    spec.addBuilder("Knee ligament_4", new tgBasicActuatorInfo(KneeLigament4Config));
    spec.addBuilder("Foot muscle", new tgBasicActuatorInfo(FootMuscleConfig));
    /* spec.addBuilder("right muscle", new tgBasicActuatorInfo(rightMuscleConfig));
    spec.addBuilder("left muscle", new tgBasicActuatorInfo(leftMuscleConfig));
    spec.addBuilder("top muscle", new tgBasicActuatorInfo(topMuscleConfig));
    spec.addBuilder("vertical muscle", new tgBasicActuatorInfo(verticalMuscleConfig));
    spec.addBuilder("horizontal muscle", new tgBasicActuatorInfo(horizontalMuscleConfig));
    //spec.addBuilder("bottom muscle", new tgBasicActuatorInfo(bottomMuscleConfig)); //overwrites previous, comment out if you dont want it to */
    
    // Create your structureInfo
    tgStructureInfo structureInfo(s, spec);

    // Use the structureInfo to build ourselves
    structureInfo.buildInto(*this, world);

    // We could now use tgCast::filter or similar to pull out the
    // models (e.g. muscles) that we want to control. 
    allMuscles = tgCast::filter<tgModel, tgBasicActuator> (getDescendants());

    // call the onSetup methods of all observed things e.g. controllers
    notifySetup();

    // Actually setup the children
    tgModel::setup(world);

    //map the rods and add the markers to them
    //addMarkers(s);

}

void LegModel::step(double dt)
{
    // Precondition
    if (dt <= 0.0) {
        throw std::invalid_argument("dt is not positive");
    } else {
        // Notify observers (controllers) of the step so that they can take action
        notifyStep(dt);
        tgModel::step(dt);  // Step any children
    }
}

void LegModel::onVisit(tgModelVisitor& r)
{
    tgModel::onVisit(r);
}

const std::vector<tgBasicActuator*>& LegModel::getAllMuscles() const
{
    return allMuscles;
}
    
void LegModel::teardown()
{
    notifyTeardown();
    tgModel::teardown();
}

