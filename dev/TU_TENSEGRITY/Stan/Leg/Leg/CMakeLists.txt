link_directories(${LIB_DIR})

link_libraries(tgcreator
                util
                sensors
                core    
                Adapters
                terrain 
                tgOpenGLSupport
                controllers core)

add_executable(AppLeg
    AppLeg.cpp
    LegModel.cpp
    LegController.cpp
)
