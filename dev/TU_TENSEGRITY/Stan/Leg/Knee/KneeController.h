/*
 * Copyright © 2015, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 * 
 * The NASA Tensegrity Robotics Toolkit (NTRT) v1 platform is licensed
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
*/

#ifndef Knee_CONTROLLER_H
#define Knee_CONTROLLER_H

/**
 * @file KneeController.h
 * @brief Contains the definition of class KneeController
 * @author Steven Lessard
 * @version 1.0.0
 * $Id$
 */

// This library
#include "core/tgObserver.h"
#include "learning/Adapters/AnnealAdapter.h"
#include <vector>
#include <ncurses.h>

// Forward declarations
class KneeModel;

//namespace std for vectors
using namespace std;

/**
 * Preferred Length Controller for KneeModel. This controllers sets a preferred rest length for the muscles.
 * Constant speed motors are used in muscles to move the rest length to the preffered length over time.
 * The assumption here is that motors are constant speed independent of the tension of the muscles.
 * motorspeed and movemotors are defined at the tgBasicActuator class.
 */
class KneeController : public tgObserver<KneeModel>
{
public:
	
  /**
   * Construct a KneeController with the initial preferred length.
   *
   */
  
  // Note that currently this is calibrated for decimeters.
	KneeController(const double prefLength, double timestep);
    
  /**
   * Nothing to delete, destructor must be virtual
   */
  virtual ~KneeController() { }

  virtual void onSetup(KneeModel& subject);
    
  virtual void onStep(KneeModel& subject, double dt);

protected:

  virtual vector< vector <double> > transformActions(vector< vector <double> > act);

  virtual void applyActions (KneeModel& subject, vector< vector <double> > act);

private:
  double m_initialLengths;
  double m_totalTime;
  double m_totalTime_2;
  double variable_1;
  double dt;
  double hamstring_1_length; // rest length hamstring 1 in cm
  double hamstring_2_length; // rest length of hamstring 2 in cm
  double quadriceps_1_length; // rest length of quadriceps in cm
  double ligament_1_length; // rest length of ligament_1 and ligament_2 in cm
  double ligament_3_length; // rest length of ligament_3 and ligament_4 in cm
  ofstream myfile;
//  AnnealAdapter evolutionAdapter;

  void setQuadricepsRestLength(KneeModel& subject, double dt);
  void setHamstringRestLength(KneeModel& subject, double dt);
  void moveAllMotors(KneeModel& subject, double dt);
  void updateActions(KneeModel& subject, double dt);
  void checkButtonPress();
  void getLengths(KneeModel& subject, double time);
};

#endif // Knee_CONTROLLER_H
