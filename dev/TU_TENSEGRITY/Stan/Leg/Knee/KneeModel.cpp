/*
 * Copyright © 2015, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 * 
 * The NASA Tensegrity Robotics Toolkit (NTRT) v1 platform is licensed
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
*/

/**
 * @file KneeModel.cpp
 * @brief Contains the implementation of class KneeModel. See README
 * $Id$
 */

// This module
#include "KneeModel.h"
// This library
#include "core/tgBasicActuator.h"
#include "core/tgRod.h"
#include "core/abstractMarker.h"
#include "tgcreator/tgBuildSpec.h"
#include "tgcreator/tgBasicActuatorInfo.h"
#include "tgcreator/tgKinematicContactCableInfo.h"
#include "tgcreator/tgRodInfo.h"
#include "tgcreator/tgStructure.h"
#include "tgcreator/tgStructureInfo.h"
// The Bullet Physics library
#include "LinearMath/btVector3.h"
// The C++ Standard Library
#include <stdexcept>

# define M_PI 3.1415926535897932384

namespace {
    // see tgBaseString.h for a descripton of some of these rod parameters
    // (specifically, those related to the motor moving the strings.)
    // NOTE that any parameter that depends on units of length will scale
    // with the current gravity scaling. E.g., with gravity as 98.1,
    // the length units below are in decimeters.

    const struct ConfigRod {
        double density;
        double radius;
        double friction;
        double rollFriction;
        double restitution;
    } cRod = {
        0.002802344,    // density (kg / length^3) adjusted so that bottom part weighs the same irl
        0.3,     // radius (length)
        1.0,      // friction (unitless)
        0.01,     // rollFriction (unitless)
        0.2      // restitution (?)
    };
    const struct ConfigStructure {
    	  double upper_offset;
    	  double lower_offset;
    	  double upper_length;
    	  double lower_length;
    	  double upper_tip_length;
    	  double lower_tip_length;
    	  double lower_z_offset;

    } cStructure= {

    		26,
			0,
			18.5,
			22,
			10,
			5,
			0,

    };
    const struct ConfigMuscle {

        double damping;
        double stiffness;
        double pretension_1;
        double pretension_2;
        double pretension_3;
        double pretension_4;
        double pretension_5;
        double pretension_6;
        double pretension_7;
        double pretension_8;
        bool   history;
        double maxTens;
        double targetVelocity;
        double mRad;
        double motorFriction;
        double motorInertia;
        bool backDrivable;
    } cMuscle = {

        60.0,         	 // damping (kg/s)
        10,         // stiffness (kg / sec^2)
        0,      	 // pretension_1
		0,      	 // pretension_2
		50,      	 // pretension_3
		50,      	 // pretension_4
        false,         	 // history (boolean)
        80000,        	 // maxTens
        10000,           // targetVelocity
        1.0,           	 // mRad
        10.0,          	 // motorFriction
        1.0,           	 // motorInertia
        false          	 // backDrivable (boolean)
    };
    const struct ConfigLigament {

        double damping;
        double stiffness;
        double pretension_1;
        double pretension_2;
        double pretension_3;
        double pretension_4;
        double pretension_5;
        double pretension_6;
        double pretension_7;
        double pretension_8;
        bool   history;  
        double maxTens;
        double targetVelocity; 
        double mRad;
        double motorFriction;
        double motorInertia;
        bool backDrivable;
    } cLigament = {

        60.0,         	 // damping (kg/s)
        10.0,         // stiffness (kg / sec^2)
        2.0,      	 // pretension_1
		2.0,      	 // pretension_2
		2.0,      	 // pretension_3
		2.0,      	 // pretension_4
        false,         	 // history (boolean)
        80000,        	 // maxTens
        10000,           // targetVelocity  
        1.0,           	 // mRad
        10.0,          	 // motorFriction
        1.0,           	 // motorInertia
        false          	 // backDrivable (boolean)
    };
} // namespace

KneeModel::KneeModel() : tgModel() {}

KneeModel::~KneeModel() {}

void KneeModel::addNodes(tgStructure& s) {
	double const x=0.01; //length of bottom rectangle

        //upper bones
	    s.addNode(0,cStructure.upper_offset+cStructure.upper_length,0);//0
	    s.addNode(0,cStructure.upper_offset, 0); //1
	    s.addNode(0,cStructure.upper_offset-(cStructure.upper_tip_length/sqrt(2)),cStructure.upper_tip_length/sqrt(2)); //2
	    s.addNode((cStructure.upper_tip_length/2)*sqrt(3)/sqrt(2),cStructure.upper_offset-(cStructure.upper_tip_length/sqrt(2)),-(cStructure.upper_tip_length/2/sqrt(2)));//3
	    s.addNode(-(cStructure.upper_tip_length/2)*sqrt(3)/sqrt(2),cStructure.upper_offset-(cStructure.upper_tip_length/sqrt(2)),-(cStructure.upper_tip_length/2/sqrt(2)));//4

	    //Lower bones
	    s.addNode(0,cStructure.lower_offset,cStructure.lower_z_offset);//5
	    s.addNode(0,cStructure.lower_offset+cStructure.lower_length,cStructure.lower_z_offset);//6
	    s.addNode(-cStructure.lower_tip_length/sqrt(2),cStructure.lower_offset+cStructure.lower_length+cStructure.lower_tip_length/sqrt(2),cStructure.lower_z_offset);//7
	    s.addNode(cStructure.lower_tip_length/sqrt(2),cStructure.lower_offset+cStructure.lower_length+cStructure.lower_tip_length/sqrt(2),cStructure.lower_z_offset);//8

	    s.addNode(x,cStructure.lower_offset,cStructure.lower_z_offset+x);//9
	    s.addNode(x,cStructure.lower_offset,cStructure.lower_z_offset-x);//10
	    s.addNode(-x,cStructure.lower_offset,cStructure.lower_z_offset+x);//11
	    s.addNode(-x,cStructure.lower_offset,cStructure.lower_z_offset-x);//12

	    s.addNode(0,cStructure.lower_offset,cStructure.lower_z_offset-4);//13
	    s.addNode(0,cStructure.lower_offset,cStructure.lower_z_offset-5);//14
	    }

                  
void KneeModel::addRods(tgStructure& s) {
    // upper bones
    s.addPair(0,  1,  "massless bone");
    s.addPair(1,  2,  "massless bone");
    s.addPair(1,  3,  "massless bone");
    s.addPair(1,  4,  "massless bone");

    // lower bones
    s.addPair(5,  6,  "bone");
    s.addPair(6,  7,  "bone");
    s.addPair(6,  8,  "bone");

    //base rectangle for muscle attachment
    s.addPair(5,  9,  "bone");
    s.addPair(5,  10,  "bone");
    s.addPair(5,  11,  "bone");
    s.addPair(5,  12,  "bone");

    s.addPair(13,  14,  "bone");
}

 void KneeModel::addMuscles(tgStructure& s) {
    const std::vector<tgStructure*> children = s.getChildren();
    
    //ligaments
    s.addPair(2, 8, "knee ligament_1");
    s.addPair(2, 7, "knee ligament_1");
    s.addPair(3, 8, "knee ligament_1");
    s.addPair(4, 7, "knee ligament_1");

    //muscles
    s.addPair(2, 9, "muscle_1");
    s.addPair(2, 11, "muscle_2");
    s.addPair(3, 10, "muscle_3");
    s.addPair(4, 12, "muscle_4");

    s.addPair(13, 5, "knee ligament_1");


}
 
/*
void KneeModel::addMarkers(tgStructure &s) {
    std::vector<tgRod *> rods=find<tgRod>("rod");

	for(int i=0;i<10;i++)
	{
		const btRigidBody* bt = rods[rodNumbersPerNode[i]]->getPRigidBody();
		btTransform inverseTransform = bt->getWorldTransform().inverse();
		btVector3 pos = inverseTransform * (nodePositions[i]);
		abstractMarker tmp=abstractMarker(bt,pos,btVector3(0.08*i,1.0 - 0.08*i,.0),i);
		this->addMarker(tmp);
	}
}
*/
 
void KneeModel::setup(tgWorld& world) {
    const tgRod::Config boneConfig(cRod.radius, cRod.density, cRod.friction, cRod.rollFriction, cRod.restitution);
    const tgRod::Config boneMasslessConfig(cRod.radius, 0, cRod.friction, cRod.rollFriction, cRod.restitution);
    /// @todo acceleration constraint was removed on 12/10/14 Replace with tgKinematicActuator as appropriate

    tgBasicActuator::Config kneeLigament1Config(cLigament.stiffness, cLigament.damping, cLigament.pretension_1,
                                                  cLigament.history, cLigament.maxTens, cLigament.targetVelocity);
    tgBasicActuator::Config kneeLigament2Config(cLigament.stiffness, cLigament.damping, cLigament.pretension_2,
                                                  cLigament.history, cLigament.maxTens, cLigament.targetVelocity);
    tgBasicActuator::Config kneeLigament3Config(cLigament.stiffness, cLigament.damping, cLigament.pretension_3,
                                                  cLigament.history, cLigament.maxTens, cLigament.targetVelocity);
    tgBasicActuator::Config kneeLigament4Config(cLigament.stiffness, cLigament.damping, cLigament.pretension_4,
                                                  cLigament.history, cLigament.maxTens, cLigament.targetVelocity);

    tgBasicActuator::Config kneeMuscle1Config(cMuscle.stiffness, cMuscle.damping, cMuscle.pretension_1,
                                                  cMuscle.history, cMuscle.maxTens, cMuscle.targetVelocity);
    tgBasicActuator::Config kneeMuscle2Config(cMuscle.stiffness, cMuscle.damping, cMuscle.pretension_2,
                                                  cMuscle.history, cMuscle.maxTens, cMuscle.targetVelocity);
    tgBasicActuator::Config kneeMuscle3Config(cMuscle.stiffness, cMuscle.damping, cMuscle.pretension_3,
                                                  cMuscle.history, cMuscle.maxTens, cMuscle.targetVelocity);
    tgBasicActuator::Config kneeMuscle4Config(cMuscle.stiffness, cMuscle.damping, cMuscle.pretension_4,
                                                  cMuscle.history, cMuscle.maxTens, cMuscle.targetVelocity);


   /* tgBasicActuator::Config rightMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_right,
                                                 cCable.history, cCable.maxTens, cCable.targetVelocity);
    tgBasicActuator::Config leftMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_left,
                                                        cCable.history, cCable.maxTens, cCable.targetVelocity);
    tgBasicActuator::Config topMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_top,
                                                      cCable.history, cCable.maxTens, cCable.targetVelocity);
    tgBasicActuator::Config verticalMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_vertical,
                                                      cCable.history, cCable.maxTens, cCable.targetVelocity);
    tgBasicActuator::Config horizontalMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_horizontal,
                                                      cCable.history, cCable.maxTens, cCable.targetVelocity);
   // tgBasicActuator::Config bottomMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_bottom,
   //                                                     cCable.history, cCable.maxTens, cCable.targetVelocity); // this acts on all the muscles that touch the ground at the start
            */
    // Start creating the structure
    tgStructure s;
    addNodes(s);
    addRods(s);
    addMuscles(s);
    
   /* // Rotate the structure upside down, making it balance on the top triangle
    	btVector3 rotationPoint = btVector3(0, 0.75*cStructure.edge, 0); // origin
    	btVector3 rotationAxis = btVector3(1, 0, 0);  // y-axis
    	double rotationAngle = cStructure.rotation/360*2*M_PI;
    	s.addRotation(rotationPoint, rotationAxis, rotationAngle); */

    // Move the arm out of the ground
    btVector3 offset(0.0, 4.0, 0.0);
    s.move(offset);
    

    // Create the build spec that uses tags to turn the structure into a real model
    tgBuildSpec spec;
    spec.addBuilder("bone", new tgRodInfo(boneConfig));
    spec.addBuilder("massless", new tgRodInfo(boneMasslessConfig));

    spec.addBuilder("muscle_1", new tgBasicActuatorInfo(kneeMuscle1Config));
    spec.addBuilder("muscle_2", new tgBasicActuatorInfo(kneeMuscle2Config));
    spec.addBuilder("muscle_3", new tgBasicActuatorInfo(kneeMuscle3Config));
    spec.addBuilder("muscle_4", new tgBasicActuatorInfo(kneeMuscle4Config));
    spec.addBuilder("knee ligament_1", new tgBasicActuatorInfo(kneeLigament1Config));
    spec.addBuilder("knee ligament_2", new tgBasicActuatorInfo(kneeLigament2Config));
    spec.addBuilder("knee ligament_3", new tgBasicActuatorInfo(kneeLigament3Config));
    spec.addBuilder("knee ligament_4", new tgBasicActuatorInfo(kneeLigament4Config));
    /* spec.addBuilder("right muscle", new tgBasicActuatorInfo(rightMuscleConfig));
    spec.addBuilder("left muscle", new tgBasicActuatorInfo(leftMuscleConfig));
    spec.addBuilder("top muscle", new tgBasicActuatorInfo(topMuscleConfig));
    spec.addBuilder("vertical muscle", new tgBasicActuatorInfo(verticalMuscleConfig));
    spec.addBuilder("horizontal muscle", new tgBasicActuatorInfo(horizontalMuscleConfig));
    //spec.addBuilder("bottom muscle", new tgBasicActuatorInfo(bottomMuscleConfig)); //overwrites previous, comment out if you dont want it to */
    
    // Create your structureInfo
    tgStructureInfo structureInfo(s, spec);

    // Use the structureInfo to build ourselves
    structureInfo.buildInto(*this, world);

    // We could now use tgCast::filter or similar to pull out the
    // models (e.g. muscles) that we want to control. 
    allMuscles = tgCast::filter<tgModel, tgBasicActuator> (getDescendants());

    // call the onSetup methods of all observed things e.g. controllers
    notifySetup();

    // Actually setup the children
    tgModel::setup(world);

    //map the rods and add the markers to them
    //addMarkers(s);

}

void KneeModel::step(double dt)
{
    // Precondition
    if (dt <= 0.0) {
        throw std::invalid_argument("dt is not positive");
    } else {
        // Notify observers (controllers) of the step so that they can take action
        notifyStep(dt);
        tgModel::step(dt);  // Step any children
    }
}

void KneeModel::onVisit(tgModelVisitor& r)
{
    tgModel::onVisit(r);
}

const std::vector<tgBasicActuator*>& KneeModel::getAllMuscles() const
{
    return allMuscles;

}
    
void KneeModel::teardown()
{
    notifyTeardown();
    tgModel::teardown();
}

