/*
 * Copyright © 2015, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 * 
 * The NASA Tensegrity Robotics Toolkit (NTRT) v1 platform is licensed
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
*/

/**
 * @file KneeModel.cpp
 * @brief Contains the implementation of class KneeModel. See README
 * $Id$
 */

// This module
#include "KneeModel2.h"
// This library
#include "core/tgBasicActuator.h"
#include "core/tgRod.h"
#include "core/tgSphere.h"
#include "core/abstractMarker.h"
#include "tgcreator/tgBuildSpec.h"
#include "tgcreator/tgBasicActuatorInfo.h"
#include "tgcreator/tgKinematicContactCableInfo.h"
#include "tgcreator/tgSphereInfo.h"
#include "tgcreator/tgRodInfo.h"
#include "tgcreator/tgStructure.h"
#include "tgcreator/tgStructureInfo.h"
// The Bullet Physics library
#include "LinearMath/btVector3.h"
// The C++ Standard Library
#include <stdexcept>

# define M_PI 3.1415926535897932384

namespace {
    // see tgBaseString.h for a descripton of some of these rod parameters
    // (specifically, those related to the motor moving the strings.)
    // NOTE that any parameter that depends on units of length will scale
    // with the current gravity scaling. E.g., with gravity as 98.1,
    // the length units below are in decimeters.

    const struct ConfigRod {
        double density;
        double radius;
        double friction;
        double rollFriction;
        double restitution;
        double density_2;
        double radius_2;
    } cRod = {
    	0.00308852,//density (kg / length^3) adjusted so that bottom part weighs the same irl
        0.3,     // radius (length)
        1.0,      // friction (unitless)
        0.01,     // rollFriction (unitless)
        0.2,      // restitution (?)
		0.002, //0.03472121, //density of foot (represented by a sphere so that sphere mass = 18.18 gram
		0.5 		// radius of sphere
    };
    const struct ConfigStructure {
    	  double angle_1;
    	  double angle_2;
    	  double angle_3;
    	  double angle_4;
    	  double upper_length;
    	  double lower_length;
    	  double upper_lower_offset;
		  double upper_tip_length;
    	  double lower_tip_length;

    } cStructure= {

    		45, //angle between hamstring attachment bone and lower vertical bone
			135, //angle between quadriceps attachment bone and lower vertical bone
			60, //angle between hamstring attachment bones
			90, //angle between upper tips
			18.5, //length of the upper vertical bone
			22, //length of the lower vertical bone
			4, //offset between the upper and the lower bone
			5, //length of the upper bone tips
			5 //length of the lower bone tips
    };
    const struct ConfigMuscle {

        double damping;
        double stiffness;
        double stiffness2;
        double pretension_1;
        double pretension_2;
        double pretension_3;
        bool   history;
        double maxTens;
        double targetVelocity;
        double mRad;
        double motorFriction;
        double motorInertia;
        bool backDrivable;
    } cMuscle = {

        2.0,         	 // damping (kg/s)
        5,//5.67,         // stiffness ham (kg / sec^2)
		5,//5,			//stiffness quad
        0,      	 // pretension_1
		0,      	 // pretension_3
		0,      	 // pretension_4
        false,         	 // history (boolean)
        80000,        	 // maxTens
        10000,           // targetVelocity
        1.0,           	 // mRad
        10.0,          	 // motorFriction
        1.0,           	 // motorInertia
        false          	 // backDrivable (boolean)
    };
    const struct ConfigLigament {

        double damping;
        double stiffness;
        double pretension_1;
        double pretension_2;
        double pretension_3;
        bool   history;  
        double maxTens;
        double targetVelocity; 
        double mRad;
        double motorFriction;
        double motorInertia;
        bool backDrivable;
    } cLigament = {

        2.0,         	 // damping (kg/s)
        80.0,         // stiffness (kg / sec^2)
        100.0,      	 // pretension_1
		100.0,      	 // pretension_2
		50.0,      	 // pretension_3
        false,         	 // history (boolean)
        80000,        	 // maxTens
        10000,           // targetVelocity  
        1.0,           	 // mRad
        10.0,          	 // motorFriction
        1.0,           	 // motorInertia
        false          	 // backDrivable (boolean)
    };
} // namespace

KneeModel::KneeModel() : tgModel() {}

KneeModel::~KneeModel() {}

void KneeModel::addNodes(tgStructure& s) {

	//upper bones
	s.addNode(0,cStructure.lower_length+cStructure.upper_lower_offset+cStructure.upper_length,0);//0
	s.addNode(0,cStructure.lower_length+cStructure.upper_lower_offset,0);//1
	s.addNode(0,cStructure.lower_length+cStructure.upper_lower_offset-cStructure.upper_tip_length*cos(cStructure.angle_4/360*M_PI),cStructure.upper_tip_length*sin(cStructure.angle_4/360*M_PI));//2
	s.addNode(0,cStructure.lower_length+cStructure.upper_lower_offset-cStructure.upper_tip_length*cos(cStructure.angle_4/360*M_PI),-cStructure.upper_tip_length*sin(cStructure.angle_4/360*M_PI));//3

	//lower bones

	s.addNode(0,2,0);//4
	s.addNode(0,cStructure.lower_length,0);//5
	s.addNode(cStructure.lower_tip_length*sin((180-cStructure.angle_2)/180*M_PI),cStructure.lower_length+cStructure.lower_tip_length*cos((180-cStructure.angle_2)/180*M_PI),0);//6
	s.addNode(-cos(cStructure.angle_3/360*M_PI)*(cStructure.lower_tip_length*sin((cStructure.angle_1)/180*M_PI)),cStructure.lower_length-cos(cStructure.angle_3/360*M_PI)*(cStructure.lower_tip_length*cos((cStructure.angle_1)/180*M_PI)),-sin(cStructure.angle_3/360*M_PI)*(cStructure.lower_tip_length*cos((cStructure.angle_1)/180*M_PI)));//7
	s.addNode(-cos(cStructure.angle_3/360*M_PI)*(cStructure.lower_tip_length*sin((cStructure.angle_1)/180*M_PI)),cStructure.lower_length-cos(cStructure.angle_3/360*M_PI)*(cStructure.lower_tip_length*cos((cStructure.angle_1)/180*M_PI)),sin(cStructure.angle_3/360*M_PI)*(cStructure.lower_tip_length*cos((cStructure.angle_1)/180*M_PI)));//8

	//foot
	s.addNode(2,0,0);//9
	s.addNode(-2,0,0);//10
	s.addNode(0,0,2);//11
	s.addNode(0,0,-2);//12
	s.addNode(0,0,0,"foot");//13
}

void KneeModel::addRods(tgStructure& s) {
    // upper bones
    s.addPair(0,  1,  "massless bone");
    s.addPair(1,  2,  "massless bone");
    s.addPair(1,  3,  "massless bone");

    // lower bones
    s.addPair(4,  5,  "bone");
    s.addPair(5,  6,  "bone");
    s.addPair(5,  7,  "bone");
    s.addPair(5,  8,  "bone");

    //foot/tracker

    s.addPair(4,9, "massless");
    s.addPair(4,10,"massless");
    s.addPair(4,11, "massless");
    s.addPair(4,12,"massless");

}

  void KneeModel::addMuscles(tgStructure& s) {
    const std::vector<tgStructure*> children = s.getChildren();
    
    //ligaments
    s.addPair(6, 2, "knee ligament_1");
    s.addPair(6, 3, "knee ligament_1");
    s.addPair(3, 7, "knee ligament_2");
    s.addPair(2, 8, "knee ligament_2");

    //muscles
    s.addPair(0, 6, "muscle quadriceps_1");
    s.addPair(0, 7, "muscle hamstring_1");
    s.addPair(0, 8, "muscle hamstring_2");

    //strings to keep the foot in place
    s.addPair(9,13, "foot_ligament");
    s.addPair(10,13, "foot_ligament");
    s.addPair(11,13, "foot_ligament");
    s.addPair(12,13, "foot_ligament");
}
 
/*
void KneeModel::addMarkers(tgStructure &s) {
    std::vector<tgRod *> rods=find<tgRod>("rod");

	for(int i=0;i<10;i++)
	{
		const btRigidBody* bt = rods[rodNumbersPerNode[i]]->getPRigidBody();
		btTransform inverseTransform = bt->getWorldTransform().inverse();
		btVector3 pos = inverseTransform * (nodePositions[i]);
		abstractMarker tmp=abstractMarker(bt,pos,btVector3(0.08*i,1.0 - 0.08*i,.0),i);
		this->addMarker(tmp);
	}
}
*/
 
void KneeModel::setup(tgWorld& world) {
    const tgRod::Config boneConfig(cRod.radius, cRod.density, cRod.friction, cRod.rollFriction, cRod.restitution);
    const tgRod::Config boneMasslessConfig(cRod.radius, 0, cRod.friction, cRod.rollFriction, cRod.restitution);

        /// @todo acceleration constraint was removed on 12/10/14 Replace with tgKinematicActuator as appropriate

    tgBasicActuator::Config kneeLigament1Config(cLigament.stiffness, cLigament.damping, cLigament.pretension_1,
                                                  cLigament.history, cLigament.maxTens, cLigament.targetVelocity);
    tgBasicActuator::Config kneeLigament2Config(cLigament.stiffness, cLigament.damping, cLigament.pretension_2,
                                                  cLigament.history, cLigament.maxTens, cLigament.targetVelocity);
    tgBasicActuator::Config footLigament(cLigament.stiffness, cLigament.damping, cLigament.pretension_3,
                                                  cLigament.history, cLigament.maxTens, cLigament.targetVelocity);

    tgBasicActuator::Config kneeMuscle1Config(cMuscle.stiffness, cMuscle.damping, cMuscle.pretension_1,
                                                  cMuscle.history, cMuscle.maxTens, cMuscle.targetVelocity);
    tgBasicActuator::Config kneeMuscle2Config(cMuscle.stiffness, cMuscle.damping, cMuscle.pretension_2,
                                                  cMuscle.history, cMuscle.maxTens, cMuscle.targetVelocity);
    tgBasicActuator::Config kneeMuscle3Config(cMuscle.stiffness, cMuscle.damping, cMuscle.pretension_3,
                                                  cMuscle.history, cMuscle.maxTens, cMuscle.targetVelocity);

    const tgSphere::Config footConfig(cRod.radius_2,cRod.density_2);



   /* tgBasicActuator::Config rightMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_right,
                                                 cCable.history, cCable.maxTens, cCable.targetVelocity);
    tgBasicActuator::Config leftMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_left,
                                                        cCable.history, cCable.maxTens, cCable.targetVelocity);
    tgBasicActuator::Config topMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_top,
                                                      cCable.history, cCable.maxTens, cCable.targetVelocity);
    tgBasicActuator::Config verticalMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_vertical,
                                                      cCable.history, cCable.maxTens, cCable.targetVelocity);
    tgBasicActuator::Config horizontalMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_horizontal,
                                                      cCable.history, cCable.maxTens, cCable.targetVelocity);
   // tgBasicActuator::Config bottomMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_bottom,
   //                                                     cCable.history, cCable.maxTens, cCable.targetVelocity); // this acts on all the muscles that touch the ground at the start
            */
    // Start creating the structure
    tgStructure s;
    addNodes(s);
    addRods(s);
    addMuscles(s);
    
   /* // Rotate the structure upside down, making it balance on the top triangle
    	btVector3 rotationPoint = btVector3(0, 0.75*cStructure.edge, 0); // origin
    	btVector3 rotationAxis = btVector3(1, 0, 0);  // y-axis
    	double rotationAngle = cStructure.rotation/360*2*M_PI;
    	s.addRotation(rotationPoint, rotationAxis, rotationAngle); */

    // Move the arm out of the ground
    btVector3 offset(0.0, 4.0, 0.0);
    s.move(offset);
    

    // Create the build spec that uses tags to turn the structure into a real model
    tgBuildSpec spec;
    spec.addBuilder("bone", new tgRodInfo(boneConfig));
    spec.addBuilder("massless", new tgRodInfo(boneMasslessConfig));
    spec.addBuilder("foot", new tgSphereInfo(footConfig));


    spec.addBuilder("muscle quadriceps_1", new tgBasicActuatorInfo(kneeMuscle1Config));
    spec.addBuilder("muscle hamstring_1", new tgBasicActuatorInfo(kneeMuscle2Config));
    spec.addBuilder("muscle hamstring_2", new tgBasicActuatorInfo(kneeMuscle3Config));
    spec.addBuilder("knee ligament_1", new tgBasicActuatorInfo(kneeLigament1Config));
    spec.addBuilder("knee ligament_2", new tgBasicActuatorInfo(kneeLigament2Config));
    spec.addBuilder("foot_ligament", new tgBasicActuatorInfo(footLigament));


    //spec.addBuilder("knee ligament_4", new tgSphereInfo(sphereConfig));
    /* spec.addBuilder("right muscle", new tgBasicActuatorInfo(rightMuscleConfig));
    spec.addBuilder("left muscle", new tgBasicActuatorInfo(leftMuscleConfig));
    spec.addBuilder("top muscle", new tgBasicActuatorInfo(topMuscleConfig));
    spec.addBuilder("vertical muscle", new tgBasicActuatorInfo(verticalMuscleConfig));
    spec.addBuilder("horizontal muscle", new tgBasicActuatorInfo(horizontalMuscleConfig));
    //spec.addBuilder("bottom muscle", new tgBasicActuatorInfo(bottomMuscleConfig)); //overwrites previous, comment out if you dont want it to */
    
    // Create your structureInfo
    tgStructureInfo structureInfo(s, spec);

    // Use the structureInfo to build ourselves
    structureInfo.buildInto(*this, world);

    // We could now use tgCast::filter or similar to pull out the
    // models (e.g. muscles) that we want to control. 
    allMuscles = tgCast::filter<tgModel, tgBasicActuator> (getDescendants());
    pointer = tgCast::filter<tgModel, tgRod> (getDescendants());
    // call the onSetup methods of all observed things e.g. controllers
    notifySetup();

    // Actually setup the children
    tgModel::setup(world);

    //map the rods and add the markers to them
    //addMarkers(s);

}

void KneeModel::step(double dt)
{
    // Precondition
    if (dt <= 0.0) {
        throw std::invalid_argument("dt is not positive");
    } else {
        // Notify observers (controllers) of the step so that they can take action
        notifyStep(dt);
        tgModel::step(dt);  // Step any children
    }
}

void KneeModel::onVisit(tgModelVisitor& r)
{
    tgModel::onVisit(r);
}

const std::vector<tgRod*>& KneeModel::getPointer() const
{
    return pointer;
}

const std::vector<tgBasicActuator*>& KneeModel::getAllMuscles() const
{
    return allMuscles;
}

void KneeModel::teardown()
{
    notifyTeardown();
    tgModel::teardown();
}

