/*
 * Copyright © 2015, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 * 
 * The NASA Tensegrity Robotics Toolkit (NTRT) v1 platform is licensed
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

/**
 * @file KneeController.cpp
 * @brief Preferred Length Controller for KneeModel
 * @author Steven Lessard
 * @version 1.0.0
 * $Id$
 */

// This module
#include "KneeController2.h"
// This application
#include "KneeModel2.h"
// This library
#include "core/tgBasicActuator.h"
#include "core/tgSphere.h"
#include "core/tgSphere.h"
#include "tgcreator/tgSphereInfo.h"
// The C++ Standard Library
#include <cassert>
#include <stdexcept>
#include <vector>
#include <ncurses.h>
#include <iostream>
#include <fstream>

# define M_PI 3.14159265358979323846 

using namespace std;

//Constructor using the model subject and a single pref length for all muscles.
KneeController2::KneeController2(const double initialLength, double timestep) :
    m_initialLengths(initialLength),
    m_totalTime(0.0),
	m_totalTime_2(0.0),
	variable_1(0.0),
    dt(timestep),
	quadriceps_1_length(), // rest length of quadriceps in cm
	hamstring_1_length(), // rest length hamstring 1 in cm
	hamstring_2_length(), // rest length of hamstring 2 in cm
	hamstring_length(),
	quadriceps_length(),
	ligament_1_length(), // rest length of ligament_1 and ligament_2 in cm
	ligament_2_length(), // rest length of ligament_3 and ligament_4 in cm
	pretensdiff(),
	pretensdiff2(),
	stiffness(),
	stiffness2(),
	restLength(), // total difference in restlength for simulating the artificial quadriceps
	restLength2(), // total difference in reslegth to simulate the artificial hamstrings
	variable(),
	myfile () {}



//Fetch all the muscles and set their preferred restlength
void KneeController2::onSetup(KneeModel& subject) {
	this->m_totalTime=0.0;
	this->m_totalTime_2=0;
	this->quadriceps_1_length = 10; // rest length of quadriceps in cm
	this->hamstring_1_length = 14; // rest length hamstring 1 in cm
	this->hamstring_2_length = 14; // rest length of hamstring 2 in cm
	this->hamstring_length= hamstring_1_length;
	this->quadriceps_length= quadriceps_1_length;
	this->ligament_1_length = 2.3; // rest length of ligament_1 and ligament_2 in cm
	this->ligament_2_length = 2.8; // rest length of ligament_3 and ligament_4 in cm
	this-> pretensdiff=50; // difference in force between fully actuated and non-actuated quadriceps kg*cm/sec²
	this-> pretensdiff2=50; // difference in force between fully actuated and non-actuated hamstrings kg*cm/sec²
	this-> stiffness=10; // stiffness quadriceps kg/sec²
	this-> stiffness2=10; // stiffness hamstrings kg/sec²
	this->restLength=pretensdiff/stiffness;	// total difference in restlength for simulating the artificial quadriceps
	this->restLength2=pretensdiff2/stiffness2; // total difference in restlength for simulating the artificial hamstrings
	this->variable=0;



    const std::vector<tgBasicActuator*> hamstring_1 = subject.find<tgBasicActuator>("hamstring_1");
    const std::vector<tgBasicActuator*> hamstring_2 = subject.find<tgBasicActuator>("hamstring_2");
    const std::vector<tgBasicActuator*> quadriceps_1 = subject.find<tgBasicActuator>("quadriceps_1");
    const std::vector<tgBasicActuator*> ligament_1 = subject.find<tgBasicActuator>("ligament_1");
    const std::vector<tgBasicActuator*> ligament_2 = subject.find<tgBasicActuator>("ligament_2");


        for (size_t i=0; i<quadriceps_1.size(); i++) {
    		tgBasicActuator * const pMuscle = quadriceps_1[i];
    		assert(pMuscle != NULL);
    		pMuscle->setControlInput(quadriceps_1_length, dt);
        }

        // using for loops to anticipate more muscle fibers in the future
        for (size_t i=0; i<hamstring_1.size(); i++) {
    		tgBasicActuator * const pMuscle = hamstring_1[i];
    		assert(pMuscle != NULL);
    		pMuscle->setControlInput(hamstring_1_length, dt);
        }

        for (size_t i=0; i<hamstring_2.size(); i++) {
    		tgBasicActuator * const pMuscle = hamstring_2[i];
    		assert(pMuscle != NULL);
    		pMuscle->setControlInput(hamstring_2_length, dt);
        }

        for (size_t i=0; i<ligament_1.size(); i++) {
    		tgBasicActuator * const pMuscle = ligament_1[i];
    		assert(pMuscle != NULL);
    		pMuscle->setControlInput(ligament_1_length, dt);
        }

        for (size_t i=0; i<ligament_2.size(); i++) {
    		tgBasicActuator * const pMuscle = ligament_2[i];
    		assert(pMuscle != NULL);
    		pMuscle->setControlInput(ligament_2_length, dt);
        }


    const std::vector<tgBasicActuator*> muscle = subject.find<tgBasicActuator>("muscle");

    myfile.open("/home/stan/Desktop/muscleLengthsKnee.txt");

        if (myfile.is_open())
		  {
			cout << "file created" << endl;
		    myfile.close();
		  }
		else cout << "Unable to open file";

}

// Set target length of each muscle, then move motors accordingly
void KneeController2::onStep(KneeModel& subject, double dt) {
    // Update controller's internal time
    if (dt <= 0.0) { throw std::invalid_argument("dt is not positive"); }
    m_totalTime+=dt;
    if(m_totalTime>5){
    	m_totalTime_2+=dt;
    	if(m_totalTime_2>12){m_totalTime_2=0, variable+=0.5; m_totalTime=0;}}
 	if((abs(remainder(m_totalTime_2,.5))<0.00005 || m_totalTime_2<0.00005) && m_totalTime>5-dt){
    	getLengths(subject, m_totalTime_2);}
 		hamstring_length=hamstring_1_length+variable;
 		if(m_totalTime_2==0){quadriceps_length=quadriceps_1_length+m_totalTime_2;}
 		else {quadriceps_length=quadriceps_1_length+m_totalTime_2+dt;}



    setQuadricepsRestLength(subject, dt);
    setHamstringRestLength(subject, dt);
    moveAllMotors(subject, dt);
//    	cout<<"get lengths"<<endl;
    	}

 
void KneeController2::setQuadricepsRestLength(KneeModel& subject, double dt) {
//    const double amplitude = 5; // heated force with 0 strain [in Kg*cm/sec²] /stiffness [Kg/sec²]
    double newLength = 0;

    const std::vector<tgBasicActuator*> quadriceps = subject.find<tgBasicActuator>("quadriceps_1");

    for (size_t i=0; i<quadriceps.size(); i++) {
		tgBasicActuator * const pMuscle = quadriceps[i];
		assert(pMuscle != NULL);
       // cout <<"t: " << pMuscle->getRestLength() << endl;
		pMuscle->setControlInput(quadriceps_length, dt);
      /*  std::cout<<"calculating quadriceps rest length:" << newLength << "\n";
        std::cout<<"m_totalTime: " << m_totalTime << "\n";
		pMuscle->setControlInput(newLength, dt);
        cout <<"t+1: " << pMuscle->getRestLength() << endl;*/
        //cout<<"m_totalTime: "<< m_totalTime_2 << endl;
    }
}

void KneeController2::setHamstringRestLength(KneeModel& subject, double dt) {
    double newLength = 0;

    const std::vector<tgBasicActuator*> hamstring_1 = subject.find<tgBasicActuator>("hamstring_1");
    const std::vector<tgBasicActuator*> hamstring_2 = subject.find<tgBasicActuator>("hamstring_2");


    for (size_t i=0; i<hamstring_1.size(); i++) {
        tgBasicActuator * const pMuscle = hamstring_1[i];
        assert(pMuscle != NULL);
        pMuscle->setControlInput(hamstring_length, dt);
    }

    for (size_t i=0; i<hamstring_2.size(); i++) {
        tgBasicActuator * const pMuscle = hamstring_2[i];
        assert(pMuscle != NULL);
        pMuscle->setControlInput(hamstring_length, dt);
    } 
}

//Move motors for all the muscles
void KneeController2::moveAllMotors(KneeModel& subject, double dt) {
    const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();
    for (size_t i = 0; i < muscles.size(); ++i) {
		tgBasicActuator * const pMuscle = muscles[i];
		assert(pMuscle != NULL);
		pMuscle->moveMotors(dt);
	}
     
}

// Get actions from evolutionAdapter, transform them to this structure, and apply them
void KneeController2::updateActions(KneeModel& subject, double dt) {
	/*vector<double> state=getState();
	vector< vector<double> > actions;

	//get the actions (between 0 and 1) from evolution (todo)
	actions=evolutionAdapter.step(dt,state);

	//transform them to the size of the structure
	actions = transformActions(actions);

	//apply these actions to the appropriate muscles according to the sensor values
    applyActions(subject,actions);
    */
}

//Scale actions according to Min and Max length of muscles.
vector< vector <double> > KneeController2::transformActions(vector< vector <double> > actions)
{
	double min=6;
	double max=11;
	double range=max-min;
	double scaledAct;
	for(unsigned i=0;i<actions.size();i++) {
		for(unsigned j=0;j<actions[i].size();j++) {
			scaledAct=actions[i][j]*(range)+min;
			actions[i][j]=scaledAct;
		}
	}




	return actions;
}

//Pick particular muscles (according to the structure's state) and apply the given actions one by one
void KneeController2::applyActions(KneeModel& subject, vector< vector <double> > act)
{
	//Get All the muscles of the subject
	const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();

	//Check if the number of the actions match the number of the muscles
	if(act.size() != muscles.size()) {
		cout<<"Warning: # of muscles: "<< muscles.size() << " != # of actions: "<< act.size()<<endl;
		return;
	}
	//Apply actions (currently in a random order)
	for (size_t i = 0; i < muscles.size(); ++i)	{
		tgBasicActuator * const pMuscle = muscles[i];
		assert(pMuscle != NULL);
		//cout<<"i: "<<i<<" length: "<<act[i][0]<<endl;
		pMuscle->setControlInput(act[i][0]);
	}
}

/*void KneeController::checkButtonPress(){
	 initscr();
	 cbreak();
	 noecho();
	 nodelay(stdscr, TRUE);
	 scrollok(stdscr, TRUE);

	 int ch=getch();
	 if (ch != -1) {
		 cout << ch << "\n";
	 //    ungetch(ch);
	 }
} */

void KneeController2::getLengths(KneeModel& subject, double time){

			/*myfile.open("/home/stan/Desktop/muscleLengthsKnee.txt", ios::app);
			myfile << "time:  " << time<< "\n";
			myfile << "  Length          Tension          RestLength \n";

			const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();
			for (size_t i = 0; i < muscles.size(); ++i) {
					tgBasicActuator * const pMuscle = muscles[i];
					assert(pMuscle != NULL);
					myfile << i << ". "<< pMuscle->getCurrentLength()<< "          "<< pMuscle->getTension()<< "          "<< pMuscle->getRestLength()<< "\n";
				}
			myfile <<"\n";
			  myfile.close();*/

	myfile.open("/home/stan/Desktop/muscleLengthsKnee.txt", ios::app);
				myfile <<time<<" ";

				const std::vector<tgSphere*> foot = subject.find<tgSphere>("foot");
					tgSphere * const pointer=foot[foot.size()-1];
					const btVector3 com=pointer->centerOfMass();

					std::vector<double> result(3);
					for (size_t i = 0; i < 3; ++i) { result[i] = com[i];}
					myfile<<-result[0]<<" "<<result[1]-3.486<<" "<<result[3]<<" ";

					/*const std::vector<tgRod*> rods=subject.getPointer();
					tgRod*const MassPointer=rods[rods.size()-1];
					double Mass=MassPointer->mass();

					myfile<<Mass<<" ";*/


				const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();
				for (size_t i = 4; i < muscles.size()-4; ++i) {
						tgBasicActuator * const pMuscle = muscles[i];
						assert(pMuscle != NULL);
						myfile <<pMuscle->getTension()<<" "<<pMuscle->getCurrentLength()<<" "<< pMuscle->getRestLength()<<" "<< pMuscle->getCurrentLength()-pMuscle->getRestLength()<<" ";
						cout<<pMuscle->getRestLength()<<endl;
					}
				myfile <<"\n";
				  myfile.close();

}


