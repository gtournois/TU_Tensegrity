/*
 * Copyright © 2015, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 * 
 * The NASA Tensegrity Robotics Toolkit (NTRT) v1 platform is licensed
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
*/

/**
 * @file TetrahedronModel.cpp
 * @brief Contains the implementation of class TetrahedronModel. See README
 * $Id$
 */

// This module
#include "TetrahedronModel.h"
// This library
#include "core/tgBasicActuator.h"
#include "core/tgRod.h"
#include "core/abstractMarker.h"
#include "tgcreator/tgBuildSpec.h"
#include "tgcreator/tgBasicActuatorInfo.h"
#include "tgcreator/tgKinematicContactCableInfo.h"
#include "tgcreator/tgRodInfo.h"
#include "tgcreator/tgStructure.h"
#include "tgcreator/tgStructureInfo.h"
// The Bullet Physics library
#include "LinearMath/btVector3.h"
// The C++ Standard Library
#include <stdexcept>

# define M_PI 3.1415926535897932384

namespace {
    // see tgBaseString.h for a descripton of some of these rod parameters
    // (specifically, those related to the motor moving the strings.)
    // NOTE that any parameter that depends on units of length will scale
    // with the current gravity scaling. E.g., with gravity as 98.1,
    // the length units below are in decimeters.

    const struct ConfigRod {
        double density;
        double radius;
        double friction;
        double rollFriction;
        double restitution;
    } cRod = {
        0.16,    // density (kg / length^3)
        0.5,     // radius (length)
        1.0,      // friction (unitless)
        0.01,     // rollFriction (unitless)
        0.2      // restitution (?)
    };
    const struct ConfigStructure {
    	  double edge;
    	  double rotation;
    } cStructure= {

    		16.0,			// size of the edges
			180,			// rotation in degrees
    };
    const struct ConfigCable {

        double damping;
        double stiffness;
        double pretension_back;
        double pretension_right;
        double pretension_left;
        double pretension_top;
        double pretension_vertical;
        double pretension_horizontal;
        double pretension_bottom;
        bool   history;  
        double maxTens;
        double targetVelocity; 
        double mRad;
        double motorFriction;
        double motorInertia;
        bool backDrivable;
    } cCable = {

        200.0,         	 // damping (kg/s)
        3000.0,        	 // stiffness (kg / sec^2)
        3000.0,      	 // pretension_back (force), stiffness/initial length
        3000.0,  		 // pretension_right (force), stiffness/initial length
        3000.0,     	 // pretension_left (force), stiffness/initial length
        3000.0,     	 // pretension_top (force), stiffness/initial length
		3000.0,		 	 // pretension_vertical (force), stiffness/initial length
		40000.0,       	 // pretension_horizontal (force), stiffness/initial length
		3000.0, 		 // pretension_bottom (force, stiffness/initial length
        false,         	 // history (boolean)
        100000,        	 // maxTens
        10000,           // targetVelocity  
        1.0,           	 // mRad
        10.0,          	 // motorFriction
        1.0,           	 // motorInertia
        false          	 // backDrivable (boolean)
    };
} // namespace

TetrahedronModel::TetrahedronModel() : tgModel() {}

TetrahedronModel::~TetrahedronModel() {}

void TetrahedronModel::addNodes(tgStructure& s) {


	    //bottom hexagon starting top left rotating clockwise
	    s.addNode(0.5*cStructure.edge, 0, 0.5*cStructure.edge*sqrt(3)); //0
	    s.addNode(-0.5*cStructure.edge, 0, 0.5*cStructure.edge*sqrt(3)); //1
	    s.addNode(-cStructure.edge, 0, 0); //2
	    s.addNode(-0.5*cStructure.edge, 0, -cStructure.edge*0.5*sqrt(3)); //3
	    s.addNode(0.5*cStructure.edge, 0, -cStructure.edge*0.5*sqrt(3)); //4
	    s.addNode(cStructure.edge, 0, 0); //5
	    //middle layer triangle starting at top rotating clockwise
	    s.addNode(0, 0.5*sqrt(3)*cStructure.edge, 0.5*cStructure.edge*sqrt(3)+0.5*cStructure.edge/sqrt(3)); //6
	    s.addNode(-cStructure.edge, 0.5*sqrt(3)*cStructure.edge, -cStructure.edge/sqrt(3)); //7
	    s.addNode(cStructure.edge, 0.5*sqrt(3)*cStructure.edge, -cStructure.edge/sqrt(3)); //8
	    // Top layer triangle starting at top rotating clockwise
	    s.addNode(0, 1.5*cStructure.edge, cStructure.edge/(sqrt(3))); //9
	    s.addNode(-0.5*cStructure.edge, 1.5*cStructure.edge, -0.5*cStructure.edge/sqrt(3)); //10
	    s.addNode(0.5*cStructure.edge, 1.5*cStructure.edge, -0.5*cStructure.edge/sqrt(3)); //11
	    }

                  
void TetrahedronModel::addRods(tgStructure& s) {
    // base
    s.addPair(0,  7,  "rod");
    s.addPair(2,  8,  "rod");
    s.addPair(4,  6,  "rod");
    s.addPair(1,  11,  "rod");
    s.addPair(5,  10,  "rod");
    s.addPair(3,  9,  "rod");
}

 void TetrahedronModel::addMuscles(tgStructure& s) {
    const std::vector<tgStructure*> children = s.getChildren();
    
    //back triangle at bottom
    s.addPair(0, 1, "back bottom muscle");
    s.addPair(1, 6, "back muscle");
    s.addPair(6, 0, "back muscle");

    //right triangle at bottom
    s.addPair(2, 3, "right bottom muscle");
    s.addPair(3, 7, "right muscle");
    s.addPair(7, 2, "right muscle");

    //left triangle at bottom
    s.addPair(4, 5, "left bottom muscle");
    s.addPair(5, 8, "left muscle");
    s.addPair(8, 4, "left muscle");

    //top triangle
    s.addPair(9, 10, "top muscle");
    s.addPair(10, 11, "top muscle");
    s.addPair(11, 9, "top muscle");

    //vertical muscles
    s.addPair(6, 9, "vertical muscle");
    s.addPair(7, 10, "vertical muscle");
    s.addPair(8, 11, "vertical muscle");

    //horizontal muscles
    s.addPair(1, 2, "horizontal bottom muscle");
    s.addPair(3, 4, "horizontal bottom muscle");
    s.addPair(5, 0, "horizontal bottom muscle");

}
 
/*
void TetrahedronModel::addMarkers(tgStructure &s) {
    std::vector<tgRod *> rods=find<tgRod>("rod");

	for(int i=0;i<10;i++)
	{
		const btRigidBody* bt = rods[rodNumbersPerNode[i]]->getPRigidBody();
		btTransform inverseTransform = bt->getWorldTransform().inverse();
		btVector3 pos = inverseTransform * (nodePositions[i]);
		abstractMarker tmp=abstractMarker(bt,pos,btVector3(0.08*i,1.0 - 0.08*i,.0),i);
		this->addMarker(tmp);
	}
}
*/
 
void TetrahedronModel::setup(tgWorld& world) {
    const tgRod::Config rodConfig(cRod.radius, cRod.density, cRod.friction, cRod.rollFriction, cRod.restitution);
    //const tgRod::Config rodConfigMassless(cRod.radius, 0.00/*c.density*/, cRod.friction, cRod.rollFriction, cRod.restitution);
    /// @todo acceleration constraint was removed on 12/10/14 Replace with tgKinematicActuator as appropreate

    tgBasicActuator::Config backMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_back,
                                                  cCable.history, cCable.maxTens, cCable.targetVelocity);
    tgBasicActuator::Config rightMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_right,
                                                 cCable.history, cCable.maxTens, cCable.targetVelocity);
    tgBasicActuator::Config leftMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_left,
                                                        cCable.history, cCable.maxTens, cCable.targetVelocity);
    tgBasicActuator::Config topMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_top,
                                                      cCable.history, cCable.maxTens, cCable.targetVelocity);
    tgBasicActuator::Config verticalMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_vertical,
                                                      cCable.history, cCable.maxTens, cCable.targetVelocity);
    tgBasicActuator::Config horizontalMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_horizontal,
                                                      cCable.history, cCable.maxTens, cCable.targetVelocity);
   // tgBasicActuator::Config bottomMuscleConfig(cCable.stiffness, cCable.damping, cCable.pretension_bottom,
   //                                                     cCable.history, cCable.maxTens, cCable.targetVelocity); // this acts on all the muscles that touch the ground at the start
            
    // Start creating the structure
    tgStructure s;
    addNodes(s);
    addRods(s);
    addMuscles(s);
    
    // Rotate the structure upside down, making it balance on the top triangle
    	btVector3 rotationPoint = btVector3(0, 0.75*cStructure.edge, 0); // origin
    	btVector3 rotationAxis = btVector3(1, 0, 0);  // y-axis
    	double rotationAngle = cStructure.rotation/360*2*M_PI;
    	s.addRotation(rotationPoint, rotationAxis, rotationAngle);

    // Move the arm out of the ground
    btVector3 offset(0.0, 20.0, 0.0);
    s.move(offset);
    

    // Create the build spec that uses tags to turn the structure into a real model
    tgBuildSpec spec;
    spec.addBuilder("rod", new tgRodInfo(rodConfig));
    spec.addBuilder("back muscle", new tgBasicActuatorInfo(backMuscleConfig));
    spec.addBuilder("right muscle", new tgBasicActuatorInfo(rightMuscleConfig));
    spec.addBuilder("left muscle", new tgBasicActuatorInfo(leftMuscleConfig));
    spec.addBuilder("top muscle", new tgBasicActuatorInfo(topMuscleConfig));
    spec.addBuilder("vertical muscle", new tgBasicActuatorInfo(verticalMuscleConfig));
    spec.addBuilder("horizontal muscle", new tgBasicActuatorInfo(horizontalMuscleConfig));
    //spec.addBuilder("bottom muscle", new tgBasicActuatorInfo(bottomMuscleConfig)); //overwrites previous, comment out if you dont want it to
    
    // Create your structureInfo
    tgStructureInfo structureInfo(s, spec);

    // Use the structureInfo to build ourselves
    structureInfo.buildInto(*this, world);

    // We could now use tgCast::filter or similar to pull out the
    // models (e.g. muscles) that we want to control. 
    allMuscles = tgCast::filter<tgModel, tgBasicActuator> (getDescendants());

    // call the onSetup methods of all observed things e.g. controllers
    notifySetup();

    // Actually setup the children
    tgModel::setup(world);

    //map the rods and add the markers to them
    //addMarkers(s);

}

void TetrahedronModel::step(double dt)
{
    // Precondition
    if (dt <= 0.0) {
        throw std::invalid_argument("dt is not positive");
    } else {
        // Notify observers (controllers) of the step so that they can take action
        notifyStep(dt);
        tgModel::step(dt);  // Step any children
    }
}

void TetrahedronModel::onVisit(tgModelVisitor& r)
{
    tgModel::onVisit(r);
}

const std::vector<tgBasicActuator*>& TetrahedronModel::getAllMuscles() const
{
    return allMuscles;
}
    
void TetrahedronModel::teardown()
{
    notifyTeardown();
    tgModel::teardown();
}

