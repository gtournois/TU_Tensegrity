
# include "tuController.h"
#include "helpers/FileHelpers.h"
#include <cassert>
#include <cmath>
#include <stdexcept>
#include <iostream>
#include <vector>
#include <string>

// For user input
#include "BulletDynamics/Dynamics/btDynamicsWorld.h"
#include "tgGlutStuff.cpp"

// Other models
#include "Guido/3_prism/PrismModel.h"
#include "core/tgSpringCableActuator.h"

double m_prefHeight = 6.0;
double m_previousHeight = 6.0;
double desHeight = 6.0;
double desRadius = 0.2;
double m_input0 = 3 ;
double m_input1 = 5;
double m_input2 = 5;
const double default_input0 = 5;
const double default_input1 = 8;
const double default_input2 = 8;

double maxHeight = 8.7;
double minHeight = 1.0;
double maxRadius = 5.0; // dependent on height
double minRadius = 0.1;
double control0;
double control1;
double control2;


tuController::tuController(){
	std::cout << "AWESOME, TU Controller initialized" << std::endl;
//	gtgDemoApplication->m_dynamicsWorld->setState(gtgDemoApplication->m_dynamicsWorld->idle);
};

//void tuController::onStep(PrismModel& subject,std::vector<tgSpringCableActuator*, std::allocator<tgSpringCableActuator*> > actuators, btDynamicsWorld::STATE state, double dt){


void tuController::onStep(PrismModel& subject,std::vector<tgSpringCableActuator*> actuators, double dt){

	// FOR USER INPUT
	btDynamicsWorld::STATE state = gtgDemoApplication->m_dynamicsWorld->getState();

	int nMuscles = actuators.size();

	double error_h = 0;
	double error_c = 0;
	double gain = 0.0005;
	double step = 0.05;

	double current_height = subject.heightTensegrity();
	double current_radius = subject.diffCOM();

	switch (state){
	case 1:
	{
		if (desHeight < maxHeight){
			desHeight += step;
			std::cout << "desHeight" << desHeight << std::endl;}
		break;
	}
	case 2:
	{
		if (desHeight > minHeight){
			desHeight -= step;
			std::cout << "desHeight" << desHeight << std::endl;	}
		break;
	}
	case 3:
	{
		if (desRadius > minRadius){
			desRadius -= step;
			std::cout << "desRadius" << desRadius << std::endl;	}
		break;
	}
	case 4:
	{
		if (desRadius < maxRadius){
			desRadius += step;
			std::cout << "desRadius" << desRadius << std::endl;}
		break;
	}
	case 5:
	{
		rollFlag = true;
		std::cout << "starting to roll" << std::endl;
		break;
	}
	case 6:
	{
		break;
	}
	default:
	{
	}
	}

//	If up down radius
	error_h = current_height - desHeight;
	error_c = current_radius - desRadius;
	control0 = m_input0 + gain * error_h;
	control1 = m_input1 + 4 * gain * error_c;


	if (state == gtgDemoApplication->m_dynamicsWorld->idle){
		control0 = default_input0;
		control1 = default_input1;
		control2 = default_input2;
		desHeight = current_height;
		desRadius = current_radius;
		std::cout << "IDLE" << std::endl;
	}

//	Define clusters
//	0 0 0 0 0 0 1 1 1
//	4 0 0 4 0 0 1 1 1
//	0 0 0 0 0 0 1 1 1
//
// 	Find out the number of spring lengths
//	3 for base lengths

	for(int iMuscle=0; iMuscle < nMuscles; iMuscle++) {
		tgSpringCableActuator* pActuators = actuators[iMuscle];
//		pActuators->getDescendants();
//		assert(pActuators != NULL);
		// NO CONTROL

		if (iMuscle < 3) { // cluster 0
			pActuators->setControlInput(control0, dt);
		} else if (iMuscle >= 3 && iMuscle < 6) { //cluster 1
			pActuators->setControlInput(control0, dt);
		} else if (iMuscle >= 6 && iMuscle < 9) { //cluster2
			pActuators->setControlInput(control1, dt);
		} //else {
		//			cluster = 3; //CCW
		//			pActuators->setControlInput(10.0, dt);
		//			}
	}
	m_input0 = control0;
	m_input1 = control1;
	m_input2 = control2;

	if (state != gtgDemoApplication->m_dynamicsWorld->idle){
		gtgDemoApplication->m_dynamicsWorld->setState(gtgDemoApplication->m_dynamicsWorld->stop);
	}
}

