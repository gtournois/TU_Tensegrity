
#ifndef PRISMCONTROLLER
#define PRISMCONTROLLER

/**
 * @file PrismController.h
 * @brief Contains the definition of class PrismController.
 * @author Guido Tournois
 * @version 1.0.0
 */

#include <vector>
#include "dev/TU_TENSEGRITY/tuController.h"
#include "core/tgObserver.h"
//#include "tgGlutStuff.h"


// Forward declarations
class PrismModel;
class tgBasicActuator;

/** Escape Controller for T6 */
class PrismController : public tgObserver<PrismModel>
{
    public:
		double m_totalTime;
		int musclesPerCluster;
		int nClusters;

		tuController FirstPrismControl;
        // Note that currently this is calibrated for decimeters.
        PrismController(void); 

        /** Nothing to delete, destructor must be virtual */
        virtual ~PrismController(){};

        virtual void onSetup(PrismModel& subject);

        virtual void onStep(PrismModel& subject, double dt);

        virtual void onTeardown(PrismModel& subject);
    private:
        std::vector<double> initPosition; // Initial position of model

        /** A vector clusters, each of which contains a vector of muscles */
        std::vector<std::vector<tgBasicActuator*> > clusters;

        /** Sets target lengths for each muscle */
        void setPreferredMuscleLengths(PrismModel& subject, double dt);

        /** Divides the 12 muscles of the PrismModel
         * into 4 clusters of 3 muscles */
//        void populateClusters(PrismModel& subject);
};

#endif // PRISMCONTROLLER
