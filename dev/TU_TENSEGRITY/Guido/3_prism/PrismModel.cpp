/*
 * Copyright © 2012, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 * 
 * The NASA Tensegrity Robotics Toolkit (NTRT) v1 platform is licensed
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

/**
 * @file PrismModel.cpp
 * @brief Contains the definition of the members of the class PrismModel.
 * $Id$
 */

// This module
#include "PrismModel.h"
// This library
#include "core/tgBasicActuator.h"
#include "core/tgRod.h"
#include "tgcreator/tgBuildSpec.h"
#include "tgcreator/tgBasicActuatorInfo.h"
#include "tgcreator/tgRodInfo.h"
#include "tgcreator/tgStructure.h"
#include "tgcreator/tgStructureInfo.h"
#include "core/tgSimViewGraphics.h"
//
#include "core/tgSpringCableActuator.h"
// The Bullet Physics library
#include "LinearMath/btVector3.h"
#include "BulletDynamics/Dynamics/btRigidBody.h"
#include "BulletDynamics/Dynamics/btDynamicsWorld.h"
#include "btBulletDynamicsCommon.h"
//#include "tgDemoApplication.h"
// The C++ Standard Library
#include <stdexcept>
#include <math.h>
//#include <Eigen/Dense>
#include <Eigen/Core>
#include <iostream>

# define M_PI 3.1415926535897932384
/**
 * Anonomous namespace so we don't have to declare the config in
 * the header.
 */
namespace
{
/**
 * Configuration parameters so they're easily accessable.
 * All parameters must be positive.
 */
struct Config
	{
		double density;
		double radius;
		double stiffness;
		double damping;
		double pretension;
		double triangle_base;
		double prism_height;
		double prism_angle;
		double targetVelocity;
	} ;

	Config c;

//
//const struct Config
//{
////	double density;
////	double radius;
////	double stiffness;
////	double damping;
////	double pretension;
////	double triangle_base;
////	double prism_height;
////	double prism_angle;
////	double targetVelocity;
//}

} // namespace

PrismModel::PrismModel() :
		tgModel()
{
}

PrismModel::~PrismModel()
{
}

void PrismModel::addNodes(tgStructure& s,
		double diameter,
		double height,
		double angle)
{
	Eigen::Matrix3d R_120, R_angle;
	Eigen::Vector3d node0, node1, node2, node3, node4, node5, node6, temp;
	//	R_120 << cos(2*M_PI/3), 0,  -sin(2*M_PI/3),  sin(2*M_PI/3), 0 , cos(2*M_PI/3) , 0 ,1 ,0 ;
	R_120 << cos(2*M_PI/3), -sin(2*M_PI/3), 0, sin(2*M_PI/3),  cos(2*M_PI/3) ,0 , 0 ,0 ,1 ;
	R_angle << cos(angle),-sin(angle), 0,  sin(angle),  cos(angle),0,  0, 0 , 1 ;

	// Calculate exact positions of all the nodes, to make sure that the rods are of equal length
	// bottom hind right (BHR)
	node0 << diameter / 2 , 0 , 0;
	s.addNode(node0(0), node0(1), node0(2)); // 1
	std::cout << "N0   " << node0.transpose() << std::endl;

	// bottom hind left (BHL)
	node1 = R_120 * node0;
	s.addNode(node1(0), node1(1), node1(2)); // 2
	std::cout << "N1   " << node1.transpose() << std::endl;

	// bottom front (BF)
	node2 = R_120 * node1;
	s.addNode(node2(0), node2(1), node2(2)); // 3
	std::cout << "N2   " << node2.transpose() << std::endl;

	// top front left (TFL)
	temp << diameter/2,  0 , height ;
	node3 = R_angle * temp;
	s.addNode(node3(0), node3(1), node3(2)); // 4
	std::cout << "N3   " << node3.transpose() << std::endl;

	// top left
	node4 = R_120 * node3;
	s.addNode(node4(0), node4(1), node4(2)); // 5
	std::cout << "N4   " << node4.transpose() << std::endl;

	// top front
	node5 = R_120 * node4;
	s.addNode(node5(0), node5(1), node5(2)); // 6
	std::cout << "N5   " << node5.transpose() << std::endl;

	std::cout << "length of rod 0-3: " << (node0 - node3).norm()<<std::endl;
	std::cout << "length of rod 1-4: " << (node1 - node4).norm()<<std::endl;
	std::cout << "length of rod 2-5: " << (node2 - node5).norm()<<std::endl;
}

void PrismModel::addRods(tgStructure& s)
{
	s.addPair( 0,  3, "rod");
	s.addPair( 1,  4, "rod");
	s.addPair( 2,  5, "rod");
}

void PrismModel::addMuscles(tgStructure& s)
{
	// Bottom Triangle
	s.addPair(0, 1,  "muscle");
	s.addPair(1, 2,  "muscle");
	s.addPair(2, 0,  "muscle");

	// Top
	s.addPair(3, 4,  "muscle");
	s.addPair(4, 5,  "muscle");
	s.addPair(5, 3,  "muscle");

	//Edges
	s.addPair(0, 5,  "muscle");
	s.addPair(1, 3,  "muscle");
	s.addPair(2, 4,  "muscle");

//	//Other new edges for rolling
//	s.addPair(0, 4,  "muscle");
//	s.addPair(1, 5,  "muscle");
//	s.addPair(2, 3,  "muscle");
}


void PrismModel::read_params(std::string File_name){
	boost::property_tree::ptree c3p;
	boost::property_tree::read_ini(File_name,c3p);
//	c =
//	{
//			0.2,     // density (mass / length^3) 0.2
//			0.3,     // radius (length)
//			500.0,   // stiffness (mass / sec^2)
//			50.0,     // damping (mass / sec)
//			500.0,    // pretension (mass * length / sec^2) 800
//			10.0,     // triangle_base (length)
//			10.0,     // prism_height (length)
//			0.37,	     // prism_angle (rad)
//			1000.0	 // target velocity
//	};

	c.density =	c3p.get<float>("Rod.density");
	c.radius  = c3p.get<float>("Rod.radius");
	c.stiffness = c3p.get<float>("Muscle.stiffness");
	c.damping = c3p.get<float>("Muscle.damping");
	c.targetVelocity = c3p.get<float>("Muscle.target_velocity");
	c.prism_angle = c3p.get<float>("Structure.prism_angle");
	c.triangle_base = c3p.get<float>("Structure.triangle_base");
	c.prism_height = c3p.get<float>("Structure.prism_height");
	c.pretension = c3p.get<float>("Muscle.pretension");
}

void PrismModel::setup(tgWorld& world)
{
	std::string Config_file = "config.ini";
	read_params(Config_file);

	// Define the configurations of the rods and strings
	// Note that pretension is defined for this string
	const tgRod::Config rodConfig(c.radius, c.density);
	const tgSpringCableActuator::Config muscleConfig(c.stiffness, c.damping, c.pretension);

	// Create a structure that will hold the details of this model
	tgStructure s;

	// Add nodes to the structure
	addNodes(s, c.triangle_base, c.prism_height, c.prism_angle);

	// Add rods to the structure
	addRods(s);

	// Add muscles to the structure
	addMuscles(s);

	// Rotate the structure 90 degrees in order to have the base triangle on the ground
	btVector3 rotationPoint = btVector3(0, c.prism_height/2, 0); // origin
	btVector3 rotationAxis = btVector3(1, 0, 0);  // y-axis
	double rotationAngle = M_PI/2;
	s.addRotation(rotationPoint, rotationAxis, rotationAngle);

	// Move the structure so it doesn't start in the ground
	s.move(btVector3(0, 5, 0));

	// Create the build spec that uses tags to turn the structure into a real model
	tgBuildSpec spec;
	spec.addBuilder("rod", new tgRodInfo(rodConfig));
	spec.addBuilder("muscle", new tgBasicActuatorInfo(muscleConfig));

	// Create your structureInfo
	tgStructureInfo structureInfo(s, spec);

	// Use the structureInfo to build ourselves
	structureInfo.buildInto(*this, world);

	// We could now use tgCast::filter or similar to pull out the
	// models (e.g. muscles) that we want to control.
	allActuators = tgCast::filter<tgModel, tgSpringCableActuator> (getDescendants());
	allRods = tgCast::filter<tgModel, tgRod> (getDescendants());

	// Notify controllers that setup has finished.
	notifySetup();

	// Actually setup the children
	tgModel::setup(world);
}

void PrismModel::step(double dt)
{
	// Precondition
	if (dt <= 0.0)
	{
		throw std::invalid_argument("dt is not positive");
	}
	else
	{
		// Notify observers (controllers) of the step so that they can take action
		notifyStep(dt);
		tgModel::step(dt);  // Step any children
	}
}

void PrismModel::onVisit(tgModelVisitor& r)
{
	// Example: m_rod->getRigidBody()->dosomething()...
	tgModel::onVisit(r);
}

const std::vector<tgSpringCableActuator*>& PrismModel::getAllActuators() const
{
	return allActuators;
}

const std::vector<tgRod*>& PrismModel::getAllRods() const
{
	return allRods;
}

double PrismModel::heightTensegrity(){
	std::vector<tgRod*> rods = getAllRods();
	Eigen::Vector3d Heights;
	Eigen::Vector3d R, CoM, begin, end;
//	btVector3 begin, end;
//	Eigen::Matrix3d R;
	for(int iRod=0; iRod < rods.size(); iRod++) {
		tgRod* const pRod = rods[iRod];
		assert(pRod != NULL);
		btVector3 CoMtmp = pRod->centerOfMass();
		Eigen::Vector3d R = pRod->getRotation();
		double LpRod = pRod->length();

		// Transform CoM variables to Eigen, since it works better
		CoM << CoMtmp[0] ,CoMtmp[1], CoMtmp[2];

		begin = CoM + LpRod/2*R;
		end   = CoM - LpRod/2*R;
//
//		if (iRod == 1){
////		std::cout <<  "Begin " <<begin.transpose() << " End " << end.transpose() << std::endl;
//		std::cout << "Com " <<CoM.transpose() << " L " << LpRod << std::endl;
//		}
		Heights(iRod) = std::max(begin(1), end(1));
		}
	double groundOffSet = 0;
	double results = Heights.sum()/3;
	return results - groundOffSet;
}

double PrismModel::diffCOM(){
	std::vector<tgRod*> rods = getAllRods();
	Eigen::Vector3d Eigen_rodCoM, Eigen_CoM;
	double distance;
	std::vector<double> CoM = getCOM();
	Eigen_CoM << CoM[0] ,CoM[1], CoM[2];
	for(int iRod=0; iRod < rods.size(); iRod++) {
		tgRod* const pRod = rods[0];
		assert(pRod != NULL);
		btVector3 rodCoM = pRod->centerOfMass();
	// Transform CoM variables to Eigen, since it works better
		Eigen_rodCoM << rodCoM[0] ,rodCoM[1], rodCoM[2];
		distance += (Eigen_CoM - Eigen_rodCoM).norm()/3;
	}
	return distance;
}



const std::vector<tgBasicActuator*>& PrismModel::getAllMuscles() const
{
	return allMuscles;
}


void PrismModel::teardown()
{
	notifyTeardown();
	tgModel::teardown();
}

std::vector<double> PrismModel::getCOM() {
	std::vector <tgRod*> rods = find<tgRod>("rod");
	assert(!rods.empty());

	btVector3 ballCenterOfMass(0, 0, 0);
	double ballMass = 0.0;
	for (std::size_t i = 0; i < rods.size(); i++) {
		const tgRod* const rod = rods[i];
		assert(rod != NULL);
		const double rodMass = rod->mass();
		const btVector3 rodCenterOfMass = rod->centerOfMass();
		ballCenterOfMass += rodCenterOfMass * rodMass;
		ballMass += rodMass;
	}

	assert(ballMass > 0.0);
	ballCenterOfMass /= ballMass;

	//    Copy to the result std::vector
	std::vector<double> result(3);
	for (size_t i = 0; i < 3; ++i) { result[i] = ballCenterOfMass[i]; }

	return result;
}









