/*
 * Copyright © 2012, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * The NASA Tensegrity Robotics Toolkit (NTRT) v1 platform is licensed
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

/**
 * @file PrismController.cpp
 * @brief Controller for 3Prism
 * @author Guido Tournois
 * @version 1.0.0
 * $Id$
 */

// This module
#include "PrismController.h"

// This application
#include "PrismModel.h"

// This library
#include "core/tgBasicActuator.h"
#include "core/tgSpringCableActuator.h"
// File helpers to use resources folder
#include "helpers/FileHelpers.h"
// The C++ Standard Library
#include <cassert>
#include <cmath>
#include <stdexcept>
#include <iostream>
#include <vector>
#include <string>
#include "dev/TU_TENSEGRITY/tuController.h"


# define M_PI 3.1415926535897932384

//Constructor using the model subject and a single pref length for all muscles.
//Currently calibrated to decimeters
PrismController::PrismController(void) :
m_prefHeight(8.0),
timeStamp(0.0),
desRadius(0.4),
flagPrefChange(false),
m_previousHeight(8.0),
desHeight(6.0),
m_totalTime(0.0),
musclesPerCluster(3),
nClusters(3), //BE CAREFULL
m_input0(5),
m_input1(5),
m_input2(8),
default_input0(5),
default_input1(5),
default_input2(8)
{
	clusters.resize(nClusters);
	for (int i=0; i<nClusters; i++) {
		clusters[i].resize(musclesPerCluster);
	}
}

/** Set the lengths of the muscles and initialize the learning adapter */
void PrismController::onSetup(PrismModel& subject)
{
	m_totalTime = 0;
	double dt = 0.0001;

	m_previousHeight = subject.heightTensegrity();
	m_prefHeight     = subject.heightTensegrity();
	desHeight		 = subject.heightTensegrity();
	//	SDL_Event* event = NULL;
	//	SDL_Init(SDL_INIT_EVENTS);


	const std::vector<tgSpringCableActuator*> actuators = subject.getAllActuators();

	for (size_t i = 0; i < actuators.size(); ++i) {
		tgSpringCableActuator * const pActuator = actuators[i];
		//    	tgBasicActuator * const pMuscle = muscles[i];
		assert(pActuator != NULL);
		//		pActuator->setControlInput(this->m_prefLength, dt);
		//		std::cout << "ControlInput" << m_prefLength <<std::endl;
	}
	//    std::cout << "Number of muscles" << muscles.size() << std::endl;

	populateClusters(subject);

}

// Set target length of each muscle, then move motors accordingly
void PrismController::onStep(PrismModel& subject, double dt)
{
	if (dt <= 0.0) {
		throw std::invalid_argument("dt is not positive");
	}
	m_totalTime+=dt;

	setPreferredMuscleLengths(subject, dt);
}
//	const std::vector<tgBasicActuator*> muscles = subject.getAllMuscles();
//	const std::vector<tgSpringCableActuator*> actuators = subject.getAllActuators();
//
//	//Move motors for all the muscles
//	for (size_t i = 0; i < muscles.size(); ++i) {
//		tgBasicActuator * const pMuscle = muscles[i];
//		assert(pMuscle != NULL);
//		pMuscle->moveMotors(dt);
//		std::cout << "test" << std::endl;
//	}
//}


void PrismController::onTeardown(PrismModel& subject) {
	std::cout << "Tearing down" << std::endl;
}

void PrismController::setPreferredMuscleLengths(PrismModel& subject, double dt) {
	//    double phase = 0; // Phase of cluster1

	const std::vector<tgSpringCableActuator*> actuators = subject.getAllActuators();
	int nMuscles = actuators.size();

	//	enum keys {
	//		KEY_PRESS_DOWN,
	//		KEY_PRESS_UP,
	//		NOTHING
	//	};

	double control0;
	double control1;
	double control2;
	double error_h = 0;
	double error_c = 0;
	double timeSpan = 3;

	double gain = 0.0005;
	double current_height = subject.heightTensegrity();


	if (m_totalTime < 30){
		control0 = default_input0;
		control1 = default_input1;
		control2 = default_input2;
//		m_previousHeight = current_height;
//		m_prefHeight = current_height;
	} else {
		if (m_totalTime < 15 && !flagPrefChange){
			setPrefHeight(8);
			setPrefRadius(0.4);
			flagPrefChange = true;
		} else if (m_totalTime < 25 && !flagPrefChange){
			setPrefHeight(8.5);
			setPrefRadius(0.4);
			flagPrefChange = true;
		} else if (m_totalTime > 25 && !flagPrefChange){
			setPrefHeight(7);
			setPrefRadius(0.5);
			flagPrefChange = true;
		}

		if (flagPrefChange){
			if (m_totalTime - timeStamp < timeSpan){
				desHeight = m_previousHeight + (m_totalTime - timeStamp)/timeSpan * (m_prefHeight - m_previousHeight);
			} else {
				flagPrefChange  = false;
			}
		}

		error_h = current_height - desHeight;
		//	std::cout << error << std::endl;
		error_c = desRadius - subject.diffCOM();

		control0 = m_input0 + gain * error_h;
		control1 = m_input1 + gain * error_h;
		control2 = m_input2 - 4 * gain * error_c;
		std::cout << m_totalTime << " CH "<< current_height << " prefH "<<m_prefHeight << " prevH "<< m_previousHeight<< " desH " << desHeight << " err_c " << error_c <<" err_h " << error_h <<  std::endl;
	}


	for(int iMuscle=0; iMuscle < nMuscles; iMuscle++) {
		tgSpringCableActuator *const pActuators = actuators[iMuscle];
		pActuators->getDescendants();
		assert(pActuators != NULL);
		// NO CONTROL
		if (iMuscle < 3) { // cluster 0
			pActuators->setControlInput(control0, dt);
		} else if (iMuscle >= 3 && iMuscle < 6) { //cluster 1
			pActuators->setControlInput(control1, dt);
		} else if (iMuscle >= 6 && iMuscle < 9) { //cluster2
			pActuators->setControlInput(control2, dt);
		} //else {
		//			cluster = 3; //CCW
		//			pActuators->setControlInput(10.0, dt);
		//			}
	}
	m_input0 = control0;
	m_input1 = control1;
	m_input2 = control2;
}

void PrismController::populateClusters(PrismModel& subject) {
	for(int cluster=0; cluster < nClusters; cluster++) {
		std::ostringstream ss;
		ss << (cluster + 1);
		std::string suffix = ss.str();
		std::vector <tgBasicActuator*> musclesInThisCluster = subject.find<tgBasicActuator>("muscle cluster" + suffix);
		clusters[cluster] = std::vector<tgBasicActuator*>(musclesInThisCluster);
	}
}

void PrismController::setPrefHeight(double pheight){
	timeStamp = m_totalTime;
	m_previousHeight = m_prefHeight;
	m_prefHeight = pheight;
}

void PrismController::setPrefRadius(double pradius){
	desRadius  = pradius;
}

