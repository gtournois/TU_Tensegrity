/*
 * Copyright © 2012, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * The NASA Tensegrity Robotics Toolkit (NTRT) v1 platform is licensed
 * under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0.
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

/**
 * @file PrismController.cpp
 * @brief Controller for 3Prism
 * @author Guido Tournois
 * @version 1.0.0
 * $Id$
 */

// This module
#include "PrismController.h"

#include "PrismModel.h"

// This library
#include "core/tgBasicActuator.h"
#include "core/tgSpringCableActuator.h"
#include "BulletDynamics/Dynamics/btDynamicsWorld.h"
// File helpers to use resources folder
#include "helpers/FileHelpers.h"
//#include ""
// The C++ Standard Library
#include <cassert>
#include <cmath>
#include <stdexcept>
#include <iostream>
#include <vector>
#include <string>
#include <Eigen/Core>


# define M_PI 3.141592653589793238

//Constructor using the model subject and a single pref length for all muscles.
//Currently calibrated to decimeters
PrismController::PrismController(void) :
	m_totalTime(0.0),
musclesPerCluster(3),
		nClusters(3) //BE CAREFULL
{
	clusters.resize(nClusters);
	for (int i=0; i<nClusters; i++) {
		clusters[i].resize(musclesPerCluster);
	}
}

/** Set the lengths of the muscles and initialize the learning adapter */
void PrismController::onSetup(PrismModel& subject)
{
	m_totalTime = 0;
	double dt = 0.0001;
	//	SDL_Event* event = NULL;
	//	SDL_Init(SDL_INIT_EVENTS);


	const std::vector<tgSpringCableActuator*> actuators = subject.getAllActuators();

	for (size_t i = 0; i < actuators.size(); ++i) {
		tgSpringCableActuator * const pActuator = actuators[i];
		//    	tgBasicActuator * const pMuscle = muscles[i];
		assert(pActuator != NULL);
		//		pActuator->setControlInput(this->m_prefLength, dt);
		//		std::cout << "ControlInput" << m_prefLength <<std::endl;
	}
	//    std::cout << "Number of muscles" << muscles.size() << std::endl;

//	populateClusters(subject);
}

// Set target length of each muscle, then move motors accordingly
void PrismController::onStep(PrismModel& subject, double dt)
{
	if (dt <= 0.0) {
		throw std::invalid_argument("dt is not positive");
	}
	m_totalTime+=dt;

//	btDynamicsWorld::STATE test = gtgDemoApplication->m_dynamicsWorld->getState();
//	std::cout << test << std::endl;


	const std::vector<tgSpringCableActuator*> actuators = subject.getAllActuators();

	FirstPrismControl.onStep(subject, actuators,  dt);
}


void PrismController::onTeardown(PrismModel& subject) {
	std::cout << "Tearing down" << std::endl;
}

void PrismController::setPreferredMuscleLengths(PrismModel& subject, double dt) {

}

//void PrismController::populateClusters(PrismModel& subject) {
//	for(int cluster=0; cluster < nClusters; cluster++) {
//		std::ostringstream ss;
//		ss << (cluster + 1);
//		std::string suffix = ss.str();
//		std::vector <tgBasicActuator*> musclesInThisCluster = subject.find<tgBasicActuator>("muscle cluster" + suffix);
//		clusters[cluster] = std::vector<tgBasicActuator*>(musclesInThisCluster);
//	}
//}

